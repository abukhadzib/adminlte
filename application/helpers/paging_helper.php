<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
function paging($jumHal,$noHal,$clink,$jumRecord,$limit_page,$dest_div='')
{
    $cRet = '<ul class="pagination" style="padding-top: 20px;">';
    
    $rec1 = $noHal<=$jumHal ? ($noHal-1)*$limit_page+1 : 0;
    $rec2 = $noHal<=$jumHal ? $noHal*$limit_page : 0;
    $rec2 = $rec2>$jumRecord ? $jumRecord : $rec2;
    
    // tampilkan jumlah record
    $cRet .= "<li class='disabled'><a href='#'>Record <span id='span-pagination-record-min'>$rec1</span> - <span id='span-pagination-record-max'>$rec2</span> of <span id='span-pagination-jumlah-record'>$jumRecord</span></a></li>";
    
    // Hal Go Top      
    if ($noHal > 1) $cRet .= "<li><a href='".$clink."/1' class='link-pagination' data-jumlah-record='".$jumRecord."' data-hal='1' data-per-hal='".$limit_page."' data-url='".$clink."' data-div='".$dest_div."'>&lt;&lt;</a></li>";
    
    // Hal Sebelumnya       
    if ($noHal > 1) $cRet .= "<li><a href='".$clink."/".($noHal-1)."' class='link-pagination' data-jumlah-record='".$jumRecord."' data-hal='".($noHal-1)."' data-per-hal='".$limit_page."' data-url='".$clink."' data-div='".$dest_div."'>&lt;</a></li>";
    
    // memunculkan nomor halaman dan linknya
    $showPage = 0;    
    for($page = 1; $page <= $jumHal; $page++)
    {
         if ((($page >= $noHal - 3) && ($page <= $noHal + 3)) || ($page == 1) || ($page == $jumHal)) 
         {   
            if (($showPage == 1) && ($page != 2))  $cRet .=  "<li class='disabled'><a href='#'>...</a></li>"; 
            if (($showPage != ($jumHal - 1)) && ($page == $jumHal))  $cRet .= "<li class='disabled'><a href='#'>...</a></li>";
            if ($page == $noHal) $cRet .= ($jumHal==1) ? "" : "<li class='active'><a href='#' style='z-index: auto;'>".$page."</a></li> ";
            else $cRet .= "<li><a href='".$clink."/".$page."' class='link-pagination' data-jumlah-record='".$jumRecord."' data-hal='".$page."' data-per-hal='".$limit_page."' data-url='".$clink."' data-div='".$dest_div."'>".$page."</a></li>";
            $showPage = $page;          
         }
    }

    // Hal Berikutnya
    if ($noHal < $jumHal) $cRet .= "<li><a href='".$clink."/".($noHal+1)."' class='link-pagination' data-jumlah-record='".$jumRecord."' data-hal='".($noHal+1)."' data-per-hal='".$limit_page."' data-url='".$clink."' data-div='".$dest_div."'>&gt;</a></li>";
    
    // Hal Go Bottom
    if ($noHal < $jumHal) $cRet .= "<li><a href='".$clink."/".$jumHal."' class='link-pagination' data-jumlah-record='".$jumRecord."' data-hal='".$jumHal."' data-per-hal='".$limit_page."' data-url='".$clink."' data-div='".$dest_div."'>&gt;&gt;</a></li>";
    
    $cRet    .= "</ul>";
    return $cRet;
}