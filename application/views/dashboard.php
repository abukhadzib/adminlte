<section class="content">
<div class="row">
	<div class="col-xs-12">
		<h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-desktop"></i> Home </h3>
	</div>
    <div class="col-xs-12">
        <div class="row">
            <?php if ( $this->isAdmin=='Y') { ?>
            
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
               <a href="<?=site_url('data_master/partner')?>">
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?=$partner_count?></h3>
                  <p>Partner</p>
                </div>
                <div class="icon">
                  <i class="fa fa-users"></i>
                </div>
                <i class="small-box-footer">
                  More info <i class="fa fa-arrow-circle-right"></i>
                </i>
              </div>
                </a>
            </div><!-- ./col -->
            
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <a href="<?=site_url('data_master/program')?>">
              <div class="small-box bg-green">
                <div class="inner">
                  <h3><?=$program_count?></h3>
                  <p>Program</p>
                </div>
                <div class="icon">
                  <i class="fa fa-gamepad"></i>
                </div>
                <i class="small-box-footer">
                  More info <i class="fa fa-arrow-circle-right"></i>
                </i>
              </div>
              </a>
            </div><!-- ./col -->
            
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <a href="<?=site_url('data_master/subprogram')?>">
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3><?=$subprogram_count?></h3>
                  <p>Subprogram</p>
                </div>
                <div class="icon">
                  <i class="fa fa-tasks"></i>
                </div>
                <i class="small-box-footer">
                  More info <i class="fa fa-arrow-circle-right"></i>
                </i>
              </div>
                </a>
            </div><!-- ./col -->
            
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <a href="<?=site_url('pesan/view')?>">
              <div class="small-box bg-red">
                <div class="inner">
                  <h3><?=$pesan_count?></h3>
                  <p>Pesan</p>
                </div>
                <div class="icon">
                  <i class="fa fa-comments"></i>
                </div>
                <i class="small-box-footer">
                  More info <i class="fa fa-arrow-circle-right"></i>
                </i>
              </div>
            </a>
            </div><!-- ./col -->
            
            <!--
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-comments fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"><?=$partner_count?></div>
                                <div>Partner</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?=site_url('data_master/partner')?>">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-tasks fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"><?=$program_count?></div>
                                <div>Program</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?=site_url('data_master/program')?>">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-shopping-cart fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"><?=$subprogram_count?></div>
                                <div>Sub Program</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?=site_url('data_master/subprogram')?>">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <?php } ?>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-support fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"><?=$pesan_count?></div>
                                <div>Pesan</div>
                            </div>
                        </div>
                    </div>
                    <a href="<?=site_url('pesan/view')?>">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            -->
            </div>
        </div>
    </div>
</div>  
</section>