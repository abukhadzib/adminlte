<section class="content">
    <section class="content-header">
        <h1>
          General Form Elements
          <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Forms</a></li>
          <li class="active">General Elements</li>
        </ol>
      </section>

    
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Quick Example</h3>
          </div><!-- /.box-header -->
          <!-- form start -->
          <form role="form">
            <div class="box-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
              </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div><!-- /.box -->
      </div>
    </div>
</section>

<script>

/*
function autoRefresh()
{
   window.location.reload();
}
 
setInterval('autoRefresh()', 60000); // this will reload page after every 60 secounds; Method II


(function poll() {
   setTimeout(function() {
       $.ajax({ url: "<?=site_url('chat/refresh/'.$other);?>", success: function(data) {
            //sales.setValue(data.value);
            $('#chat_box').html(data.responseXML);
           
       }, dataType: "json", complete: poll });
    }, 10000);
})();
*/
$('#sendMessage').click(function(){
        if ($('#message').val()==''){
            $('#message').focus();
            return false;
        } 
        $.post("<?=site_url('chat/insert_message');?>",
        {
            id:$('#id').val(),
            message:$('#message').val()
        },function(result){
            var result = eval('('+result+')');
            //alert(result.Msg);
            if (result.success){
                window.location.href = '<?=site_url('chat/index/'.$other);?>';
            }
        });
    });
    
</script>