
                      <!-- Conversations are loaded here -->
                      <div class="direct-chat-messages">
                        <!-- Message. Default to the left -->
                        <?php 
                        foreach ($content as $row){ 
                        if($other != ''){
                            if($name == $row->name){
                        ?>
                            <div class="direct-chat-msg">
                              <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-left">Me</span>
                                <span class="direct-chat-timestamp pull-right"><?=$row->time;?></span>
                              </div><!-- /.direct-chat-info -->
                              <img class="direct-chat-img" src="<?=$img?>user1-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
                              <div class="direct-chat-text">
                                <?=$row->comment;?>
                              </div><!-- /.direct-chat-text -->
                            </div><!-- /.direct-chat-msg -->
                            <?php }else{?>
                            <!-- Message to the right -->
                            <div class="direct-chat-msg right">
                              <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-right"><?=$row->name;?></span>
                                <span class="direct-chat-timestamp pull-left"><?=$row->time;?></span>
                              </div><!-- /.direct-chat-info -->
                              <img class="direct-chat-img" src="<?=$img?>user3-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
                              <div class="direct-chat-text">
                                <?=$row->comment;?>
                              </div><!-- /.direct-chat-text -->
                            </div><!-- /.direct-chat-msg -->
                        <?php
                            }
                        }
                        }
                        ?>
                       
                      </div><!--/.direct-chat-messages-->

                     
                      <!-- Contacts are loaded here -->
                      <div class="direct-chat-contacts">
                           <?php 
                        foreach ($user as $row){ 
                        ?>
                        <ul class="contacts-list">
                        <li>
                            <a href="<?=site_url('chat/index/'.$row->id);?>" data-user-name="<?=$row->name;?>" data-user-id="<?=$row->id;?>">
                              <img class="contacts-list-img" src="<?=$img?>user6-128x128.jpg">
                              <div class="contacts-list-info">
                                <span class="contacts-list-name">
                                  <?=$row->name;?>
                                  <small class="contacts-list-status"><?php if($row->online == '1'){?><span class="label label-sm label-success">Online</span><?php }else{?><span class="label label-sm label-danger"><i>Offline</i></span><?php } ?></small>
                                  <small class="contacts-list-date pull-right">Last Login : <?=$row->last_login;?></small>
                                </span>
                                <!--<span class="contacts-list-msg">Can I take a look at...</span>-->
                              </div><!-- /.contacts-list-info -->
                            </a>
                          </li><!-- End Contact Item -->
                          
                        </ul><!-- /.contatcts-list -->
                        <?php 
                            }
                        ?>
                      </div><!-- /.direct-chat-pane -->
                      
                   
