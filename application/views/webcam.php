
<section class="content">
<div class="row">
    <div class="col-xs-12">
		<h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw  fa-qrcode"></i> Webcam<span> > Chess </span></h3>
	</div>
    <div class="col-xs-12">
        <h3>Simple initalization with default settings</h3>
        <hr>
        <canvas style="width: auto; margin-left: 30%;"></canvas>
        <hr>
        <ul></ul>
    </div>
    
</div>  
</section>

<script type="text/javascript">
            var arg = {
                resultFunction: function(result) {
                    $('body').append($('<li>' + result.format + ': ' + result.code + '</li>'));
                }
            };
            $("canvas").WebCodeCamJQuery(arg).data().plugin_WebCodeCamJQuery.play();
        </script>