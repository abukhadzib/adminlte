<section class="content">
<div class="row">
	<div class="col-xs-12">
		<h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-comments"></i> Chat Room </h3>
	</div>
    <div class="col-xs-12">
        <div class="row">
            
           <div class="col-md-6">
                  <!-- DIRECT CHAT -->
                  <div class="box box-warning direct-chat direct-chat-warning">
                    <div class="box-header with-border">
                      <h3 class="box-title">Direct Chat</h3>
                      <div class="box-tools pull-right">
                        <span data-toggle="tooltip" title="3 New Messages" class="badge bg-yellow">3</span>
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div><!-- /.box-header -->
                    <div id="chat_box" class="box-body">
                      <!-- Conversations are loaded here -->
                      <div class="direct-chat-messages">
                        <!-- Message. Default to the left -->
                        <?php 
                        foreach ($content as $row){ 
                        if($other != ''){
                            if($name == $row->name){
                        ?>
                            <div class="direct-chat-msg">
                              <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-left">Me</span>
                                <span class="direct-chat-timestamp pull-right"><?=$row->time;?></span>
                              </div><!-- /.direct-chat-info -->
                              <img class="direct-chat-img" src="<?=$img?>user1-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
                              <div class="direct-chat-text">
                                <?=$row->comment;?>
                              </div><!-- /.direct-chat-text -->
                            </div><!-- /.direct-chat-msg -->
                            <?php }else{?>
                            <!-- Message to the right -->
                            <div class="direct-chat-msg right">
                              <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-right"><?=$row->name;?></span>
                                <span class="direct-chat-timestamp pull-left"><?=$row->time;?></span>
                              </div><!-- /.direct-chat-info -->
                              <img class="direct-chat-img" src="<?=$img?>avatar3.png" alt="message user image"><!-- /.direct-chat-img -->
                              <div class="direct-chat-text">
                                <?=$row->comment;?>
                              </div><!-- /.direct-chat-text -->
                            </div><!-- /.direct-chat-msg -->
                        <?php
                            }
                        }
                        }
                        ?>
                       
                      </div><!--/.direct-chat-messages-->

                     
                      <!-- Contacts are loaded here -->
                      <div class="direct-chat-contacts">
                           <?php 
                        foreach ($user as $row){ 
                        if($name == $row->name){
                        ?>
                        <ul class="contacts-list">
                        <li>
                            <a href="<?=site_url('chat/index/'.$row->id);?>" data-user-name="<?=$row->name;?>" data-user-id="<?=$row->id;?>">
                              <img class="contacts-list-img" src="<?=$img?>avatar5.png">
                              <div class="contacts-list-info">
                                <span class="contacts-list-name">
                                  Me
                                  <small class="contacts-list-status"><?php if($row->online == '1'){?><span class="label label-sm label-success">Online</span><?php }else{?><span class="label label-sm label-danger"><i>Offline</i></span><?php } ?></small>
                                  <small class="contacts-list-date pull-right">Last Login : <?=$row->last_login;?></small>
                                </span>
                                <!--<span class="contacts-list-msg">Can I take a look at...</span>-->
                              </div><!-- /.contacts-list-info -->
                            </a>
                          </li><!-- End Contact Item -->
                          
                        </ul><!-- /.contatcts-list -->
                        <?php 
                            }else{
                                ?>
                           <ul class="contacts-list">
                        <li>
                            <a href="<?=site_url('chat/index/'.$row->id);?>" data-user-name="<?=$row->name;?>" data-user-id="<?=$row->id;?>">
                              <img class="contacts-list-img" src="<?=$img?>avatar5.png">
                              <div class="contacts-list-info">
                                <span class="contacts-list-name">
                                  <?=$row->name;?>
                                  <small class="contacts-list-status"><?php if($row->online == '1'){?><span class="label label-sm label-success">Online</span><?php }else{?><span class="label label-sm label-danger"><i>Offline</i></span><?php } ?></small>
                                  <small class="contacts-list-date pull-right">Last Login : <?=$row->last_login;?></small>
                                </span>
                                <!--<span class="contacts-list-msg">Can I take a look at...</span>-->
                              </div><!-- /.contacts-list-info -->
                            </a>
                          </li><!-- End Contact Item -->
                          
                        </ul><!-- /.contatcts-list -->
                        <?php 
                            }
                        }
                        ?>
                      </div><!-- /.direct-chat-pane -->
                      
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                      <form action="#" method="post">
                        <div class="input-group">
                            <input type="text" name="message" id="message" placeholder="Type Message ..." class="form-control">
                          <span class="input-group-btn">
                              <button type="button" id="sendMessage" class="btn btn-warning btn-flat">Send</button>
                          </span>
                        </div>
                          <input type="hidden" name="id" id="id" value="<?=$id;?>" class="form-control">
                          <input type="hidden" name="other" id="other" value="<?=$other;?>"  class="form-control">
                            
                      </form>
                    </div><!-- /.box-footer-->
                  </div><!--/.direct-chat -->
                </div><!-- /.col -->
        </div>
    </div>
</div>  
</section>

<script>

/*
function autoRefresh()
{
   window.location.reload();
}
 
setInterval('autoRefresh()', 60000); // this will reload page after every 60 secounds; Method II


(function poll() {
   setTimeout(function() {
       $.ajax({ url: "<?=site_url('chat/refresh/'.$other);?>", success: function(data) {
            //sales.setValue(data.value);
            $('#chat_box').html(data.responseXML);
           
       }, dataType: "json", complete: poll });
    }, 10000);
})();
*/
$('#sendMessage').click(function(){
        if ($('#message').val()==''){
            $('#message').focus();
            return false;
        } 
        $.post("<?=site_url('chat/insert_message');?>",
        {
            id:$('#id').val(),
            message:$('#message').val()
        },function(result){
            var result = eval('('+result+')');
            //alert(result.Msg);
            if (result.success){
                window.location.href = '<?=site_url('chat/index/'.$other);?>';
            }
        });
    });
    
</script>