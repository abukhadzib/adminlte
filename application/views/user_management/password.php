<br />
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <form enctype="multipart/form-data" id="form-password" class="form-horizontal" method="post">
            <div class="form-group">                
                <label for="password" class="col-xs-4 control-label">Password Lama *</label>
                <div class="col-xs-8">
                    <input type="password" class="form-control" id="password_old" name="password_old" placeholder="Password Lama" autofocus>
                </div>
            </div>  
            <div class="form-group">                
                <label for="password" class="col-xs-4 control-label">Password Baru *</label>
                <div class="col-xs-8">
                    <input type="password" class="form-control" id="password_new" name="password_new" placeholder="Password Baru">
                </div>
            </div>  
            <div class="form-group">                
                <label for="password" class="col-xs-4 control-label">Konfirmasi Password *</label>
                <div class="col-xs-8">
                    <input type="password" class="form-control" id="password_konfirm" name="password_konfirm" placeholder="Konfirmasi Password">
                </div>
            </div>  
            <em>*) Wajib diisi.</em>
		</form>
	</div>  
</div>
