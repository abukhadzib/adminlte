<br />
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <form enctype="multipart/form-data" id="form-profile" class="form-horizontal" method="post">
            <div class="form-group">
                <label for="name" class="col-xs-2 control-label">Role *</label>
                <div class="col-xs-4">
                    <select name="role" class="form-control" id="role" disabled="">
						<option value="" selected="" disabled="" > --Role--</option>
                        <? if ($role) { foreach($role as $row_role) { ?>
						<option value="<?=$row_role->id?>"<?=$row_role->id==$user->role_id?' selected="selected"':''?>><?=$row_role->name?></option>
                        <? } } ?>
					</select>
                </div>
            </div> 
            <div class="form-group">
                <label for="name" class="col-xs-2 control-label">Nama *</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="name_profile" name="name_profile" placeholder="Nama" value="<?=$user->name?>" autofocus>
                </div>
                <label for="no_identitas" class="col-xs-2 control-label">No Identitas *</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="no_identitas_profile" name="no_identitas_profile" placeholder="No Identitas" value="<?=$user->no_identitas?>">
                </div>
            </div> 
            <div class="form-group">
                <label for="address1" class="col-xs-2 control-label">Alamat</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="address1_profile" name="address1_profile" placeholder="Alamat" value="<?=$user->address1?>">
                </div>
                <label for="phone" class="col-xs-2 control-label">No Telpon *</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="phone_profile" name="phone_profile" placeholder="No Telpon" value="<?=$user->phone?>">
                </div>
            </div> 
            <div class="form-group">
                <label for="zipcode" class="col-xs-2 control-label">Kode Pos</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="zipcode_profile" name="zipcode_profile" placeholder="Kode Pos" value="<?=$user->zip?>">
                </div>
                <label for="email" class="col-xs-2 control-label">Email *</label>
                <div class="col-xs-4">
                    <input type="email" class="form-control" id="email_profile" name="email_profile" placeholder="Email" value="<?=$user->email?>">
                </div>
            </div> 
            <div class="form-group">
                <label for="name" class="col-xs-2 control-label">Kota</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="city_profile" name="city_profile" placeholder="Kota" value="<?=$user->city?>">
                </div>
                <label for="username" class="col-xs-2 control-label">Username *</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="username_profile" name="username_profile" placeholder="Username" value="<?=$user->username?>">
                </div>
            </div> 
            <div class="form-group">
                <label for="address2" class="col-xs-2 control-label">Propinsi</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="address2_profile" name="address2_profile" placeholder="Propinsi" value="<?=$user->address2?>">
                </div>
            </div> 
            <div class="form-group">
                <label for="name" class="col-xs-2 control-label">Negara</label>
                <div class="col-xs-4">
                    <input type="text" class="form-control" id="country_profile" name="country_profile" placeholder="Negara" value="<?=$user->country?>">
                </div>
            </div>  
            <em>*) Wajib diisi.</em>
		</form>
	</div>  
</div>
