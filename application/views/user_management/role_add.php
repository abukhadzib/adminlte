<br />
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">		 
		<form enctype="multipart/form-data" id="form-role" class="form-horizontal" method="post">
            <div class="form-group">
                <label for="name" class="col-xs-2 control-label">Role</label>
                <div class="col-xs-8">
                    <input type="text" class="form-control" id="role_name" name="role_name" placeholder="Nama Role" autofocus>
                </div>
                <div class="col-xs-2">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="is_admin"> Is Admin
                        </label>
                    </div>
                </div>
            </div> 
            <div class="form-group">
                <label for="name" class="col-xs-2 control-label">Partner</label>
                <div class="col-xs-10">
                    <select name="partner" class="form-control" id="partner">
						<option value="" selected=""> --Partner--</option>
                        <? if ($partner) { foreach($partner as $row_partner) { ?>
						<option value="<?=$row_partner->id?>"><?=$row_partner->name?></option>
                        <? } } ?>
					</select>
                </div>
            </div>              
            <div class="form-group">
                <label for="name" class="col-xs-2 control-label">Program *</label>
                <div class="col-xs-10">
                    <select name="program[]" class="form-control" id="program" multiple="">
						<option value="" selected="" disabled="" > --Program--</option>
                        <? if ($program) { foreach($program as $row_program) { ?>
						<option value="<?=$row_program->id?>"><?=$row_program->name?></option>
                        <? } } ?>
					</select>
                </div>
            </div>    
            <div class="row" style="margin: auto;">												
            	<table class="table table-bordered table-hover">
            		<thead>
                        <tr>
            				<th style="text-align:center; width: 50%;">Menu</th>
            				<th style="text-align:center; width: 10%;">#</th>
                            <th style="text-align:center; width: 10%;">Create</th>
                            <th style="text-align:center; width: 10%;">Read</th>
                            <th style="text-align:center; width: 10%;">Update</th>
                            <th style="text-align:center; width: 10%;">Delete</th>
            			</tr>
            		</thead>
            		<tbody>
            			<?php foreach ($menu as $row){ ?>
                        <tr<?=$row->tipe=='H' ? ' style="background-color:#f0f0f0;"' : ''?>>
                            <td><?=($row->tipe=='H' ? '' : '&nbsp;&nbsp;&nbsp;&nbsp;') . $row->name;?></td>
                            <td style="text-align:center;">    
                                <? if($row->tipe!='H') { ?>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="checkAll">
                                    </label>
                                </div>
                                <? } ?>
                            </td>
                            <td style="text-align:center;">    
                               <? if($row->tipe!='H') { ?>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="create[]" class="checkRole checkCreate createCheck" value="<?=$row->id;?>">
                                    </label>
                                </div>
                                <? } ?>
                            </td>
                            <td style="text-align:center;">    
                                <? if($row->tipe!='H') { ?>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="read[]" class="checkRole checkRead readCheck" value="<?=$row->id;?>">
                                    </label>
                                </div>
                                <? } ?>
                            </td>
                            <td style="text-align:center;">    
                               <? if($row->tipe!='H') { ?>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="update[]" class="checkRole checkUpdate" value="<?=$row->id;?>" disabled="disabled">
                                    </label>
                                </div>
                                <? } ?>
                            </td>
                            <td style="text-align:center;">    
                                <? if($row->tipe!='H') { ?>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="delete[]" class="checkRole checkDelete" value="<?=$row->id;?>" disabled="disabled">
                                    </label>
                                </div>
                                <? } ?>
                            </td>
                        </tr>                         
                        <?php } ?>
            		</tbody>
            	</table>        	
            </div>
		</form>      
	</div>  
</div>
