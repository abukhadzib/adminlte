<section class="content">
<div class="row">
    <div class="col-xs-12">
		<h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-user"></i> User Management <span> > Role </span></h3>
	</div>
    <div class="col-xs-12">
        <div class="pull-right" style="margin-bottom: 14px;">
            <button type="button" class="btn btn-primary" id="tambahRole" data-toggle="modal" data-target="#dataRoleModal"><i class="fa fa-plus"></i> Tambah Data Role</button>
        </div>                
    </div>    
    <div class="col-xs-12">     
        <div class="table-responsive">												
        	<table class="table table-bordered table-hover table-striped" style="margin-bottom: auto; background: #fff">
        		<thead>
        			<tr>
        				<th style="text-align: center;width: 4%;">#</th>
        				<th style="text-align: center;width: 30%;">Name</th>
        				<th style="text-align: center;width: 28%;">Partner</th>
        				<th style="text-align: center;width: 28%;">Program</th>
        				<th style="text-align: center;width: 4%;">Admin</th>
                        <th style="text-align: center;width: 6%;">Action</th>
        			</tr>
        		</thead>
        		<tbody>
        			<?php 
                        $no = 1;
                        foreach ($content as $row){ 
                    ?>
                        <tr>
                            <td><?=($page*$perpage)+$no;?></td>
                            <td><?=$row->name;?></td>
                            <td><?=$row->partner_name;?></td>
                            <td><?=$row->program_name;?></td>
                            <td><?=$row->is_admin;?></td>
                            <td style="text-align: center;">
                                <a class="editRole" href="#" title="Edit" data-role-name="<?=$row->name;?>" data-role-id="<?=$row->id;?>" data-toggle="modal" data-target="#dataRoleModal"><i class="fa fa-edit"></i></a>&nbsp;
                                <a class="deleteRole" href="#" title="Remove" data-role-name="<?=$row->name;?>" data-role-id="<?=$row->id;?>" data-toggle="modal" data-target="#deleteRoleModal"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>                         
                    <?php 
                        $no++; 
                        }
                    ?>
        		</tbody>
        	</table>        	
        </div>     
        <div align="right" style=" margin-top: -25px;">
            <?=$pagination;?>
        </div>           
    </div>    
</div>                       
</section>
<!-- Modal Hapus -->
<div class="modal fade" id="deleteRoleModal" tabindex="-1" role="dialog" aria-labelledby="labelDeleteRole">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title" id="labelDeleteRole">Hapus Data Role</h4>
            </div>
            <div class="modal-body">  
                <i class="fa fa-lg fa-fw fa-warning"></i>Yakin mau hapus data Role <span id="namaRoleHapus"></span> ?                  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="deleteRoleModalYes" data-role-id="">Ya</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Data Tambah dan Edit -->
<div class="modal fade" id="dataRoleModal" tabindex="-1" role="dialog" aria-labelledby="labelDataRole">
    <div class="modal-dialog" role="document" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <!--button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title"><span id="labelDataRole"></span> Data Role <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/loading.gif')?>" /></span></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="dataRoleModalURL" style="display: none;"></span>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="simpanRoleModal" data-role-id="">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Notifikasi -->
<div class="modal fade" id="notifikasiRoleModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiRole">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiRole">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiRole" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiRoleOK">OK</button>
            </div>
        </div>
    </div>
</div>

<script>
    // Role
    $('#tambahRole').click(function() {
        var url = '<?=site_url('user_management/role_add')?>';
        $('#labelDataRole').text('Tambah');  
        $('#dataRoleModal .modal-body').html('');
        $('#dataRoleModal .modal-body').load(url);
        $('#dataRoleModalURL').text(url);
    });
    $('.editRole').click(function() {
        var id = $(this).attr('data-role-id');
		var url = '<?=site_url('user_management/role_edit')?>'+'/'+id;
        $('#labelDataRole').text('Rubah');
        $('#dataRoleModal .modal-body').html('');
        $('#dataRoleModal .modal-body').load(url);
        $('#dataRoleModalURL').text(url);
    });
    $(".deleteRole").click(function() {
        var name = $(this).attr('data-role-name');
        var id = $(this).attr('data-role-id');
		$('#deleteRoleModalYes').attr('data-role-id',id);
        $('#namaRoleHapus').html('<b>'+name+'</b>');
	});    
    $('#deleteRoleModalYes').click(function() {
        var id  = $(this).attr('data-role-id');
        var url = '<?=site_url('user_management/role_delete')?>'+'/'+id;        
        $.post(url,{action:'delete',id:id},
        function(result){
            var result = eval('('+result+')');
            $('#deleteRoleModal').modal('hide');
            showNotifikasi('notifikasiRoleModal',result.Msg);
            $('#statusNotifikasiRole').val(result.success);
        });        
	});
    $('body').on('click', '.checkRead', function(){
        if($(this).prop('checked')){
            $(this).parents('tr').find('.checkRole').removeAttr('disabled');
            $(this).parents('tr').find('.checkRole').not('.readCheck').not('.createCheck').prop('checked', true);
        }else{
            $(this).parents('tr').find('.checkRole').not('.readCheck').not('.createCheck').attr('disabled', 'disabled');
            $(this).parents('tr').find('.checkRole').not('.readCheck').not('.createCheck').prop('checked', false);
        } 
    });
    $('body').on('click', '.checkAll', function(){
        if ($(this).prop('checked')) {
            $(this).parents('tr').find('.checkRole').removeAttr('disabled');
            $(this).parents('tr').find('.checkRole').prop('checked', true);
        }else{
            $(this).parents('tr').find('.checkRole').prop('checked',false);
            $(this).parents('tr').find('.checkRole').not('.readCheck').not('.createCheck').attr('disabled', 'disabled');
        }
    });
    $('#simpanRoleModal').click(function(){
        if ( $('#role_name').val()=='' ){
            $('#role_name').focus();
            return false;
        }
        var url = $('#dataRoleModalURL').text();        
        $(this).attr('data-loading-text','Proses Simpan...').button('loading');         
        $.post(url,$('#form-role').serialize(),
        function(result){
            var result = eval('('+result+')');
            $('#dataRoleModal').modal('hide');
            showNotifikasi('notifikasiRoleModal',result.Msg);
            $('#statusNotifikasiRole').val(result.success);
        });   
    });
    $("#buttonNotifikasiRoleOK").click(function() {
        if ( $('#statusNotifikasiRole').val() ) {
            window.location.reload();
        } else {
            $('#simpanRoleModal').button('reset'); 
        }
	});        
    // End Role
    function showNotifikasi(idModal,pesan){
        $('#'+idModal+' .modal-body').html(pesan);
        $('#'+idModal).modal('show');
    }
    
    $(document).ajaxStart(function(){
        $("#loading-data").show();
    }).ajaxStop(function(){
        $("#loading-data").fadeOut("fast");
    $(window).resize();        
    }); 
</script>