<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?=$img;?>avatar5.png" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?=$name; ?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
        
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header" ></li>
      <?php foreach ($menu_role as $menu_row){ ?>
            <li class="treeview">
                    <a href="<?=(isset($menu_row['submenu']) ? '#' : site_url($menu_row['menu']->url))?>">
                        <i class="<?=$menu_row['menu']->icon?>"></i> <span><?=$menu_row['menu']->name?> </span><?=isset($menu_row['submenu'])?'<i class="fa fa-angle-left pull-right"></i>':''?>                   		
                    </a>
                    <?php if (isset($menu_row['submenu'])){ ?>
                            <ul class="treeview-menu">
                            <?php foreach ($menu_row['submenu'] as $submenu_row){ ?>
                                    <li>
                                    <a href="<?=site_url($submenu_row->url)?>">                        					
                                    <i class="<?=$submenu_row->icon?>"></i> <?=$submenu_row->name?>
                                    </a>
                                    </li>
                        <?php if ( $submenu_row->url=='pesan/view' && $this->uri->uri_string()=='pesan/view' ) { ?>
                        <div id="calendar" data-date="<?=isset($date1_view_pesan_mesinpesan)?$date1_view_pesan_mesinpesan:''?>" style="padding: 10px 20px;background-color: #bfc6cf;"></div>
                        <?php } ?>
                            <?php } ?> 
                            </ul>
                    <?php } ?>
            </li>
            <?php } ?> 
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>