<body class="hold-transition login-page" style="background-color: #0089db">

<div class="login-box">
      <div class="login-logo">
        <a href="<?=site_url('login')?>"><b>Admin</b>CMS</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Admin CMS Login Form</p>
        <form action="<?=site_url('login/dologin')?>" id="login-form" method="post">
             <?php if (isset($error_login) AND !empty($error_login)){ ?>
                <div class="alert alert-warning alert-dismissable">
                    <?=$error_login?>
                </div>
            <?php } ?>	
          <div class="form-group has-feedback">
             <input class="form-control" placeholder="Username" name="username" id="username" autofocus>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input class="form-control" placeholder="Password" name="password" type="password" id="password" value="">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox">
                <label>
                    <input name="remember_me" type="checkbox" value="Remember Me">Remember Me
                </label>
            </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
        </form>
        <!--
        <div class="social-auth-links text-center">
          <p>- OR -</p>
          <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
          <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>
        </div><!-- /.social-auth-links -->
        <!--
        <a href="#">I forgot my password</a><br>
        <a href="register.html" class="text-center">Register a new membership</a>
        -->
      </div><!-- /.login-box-body -->
      <div id="loading-box" class="modal overlay">
          <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div><!-- /.login-box -->
</body>
<script>
 // Login
$("#login-form").submit(function(){
    if ($("#username").val()==''){
        $("#username").focus();
        return false;
    }
    if ($("#password").val()==''){
        $("#password").focus();
        return false;
    }
});
resize_login();
if ( $('.alert').text().trim().length > 0 ){
    $('.login-panel').shake();
}

// End Login
</script>