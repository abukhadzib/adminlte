<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en-us">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="AdminLTE">
    <meta name="author" content="Syam">

    <title><?=$title;?></title>

    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?=$css?>bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=$css?>font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?=$css?>ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=$css?>AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?=$plugins;?>iCheck/square/blue.css">
    <link rel="stylesheet" href="<?=$css?>skins/_all-skins.min.css">
   
   <!-- Ion Slider -->
    <link rel="stylesheet" href="<?=$plugins;?>ionslider/ion.rangeSlider.css">
    <!-- ion slider Nice -->
    <link rel="stylesheet" href="<?=$plugins;?>ionslider/ion.rangeSlider.skinNice.css">
    <!-- bootstrap slider -->
    <link rel="stylesheet" href="<?=$plugins;?>bootstrap-slider/slider.css">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- jQuery 2.1.4 -->
    <script src="<?=$plugins;?>jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=$js;?>bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="<?=$plugins;?>iCheck/icheck.min.js"></script>
    <!-- FastClick -->
    <script src="<?=$plugins;?>fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?=$js;?>app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=$js;?>demo.js"></script>
    <!-- Ion Slider -->
    <script src="<?=$plugins;?>ionslider/ion.rangeSlider.min.js"></script>
    <!-- Bootstrap slider -->
    <script src="<?=$plugins;?>bootstrap-slider/bootstrap-slider.js"></script>
    
    <!--<script src="<?=site_url('base/js')?>"></script>  -->
  
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <?php    
    if ( empty($this->userid) ){
        $this->load->view('login'); 
    } else { 
    ?>
    <div class="wrapper" id="wrapper">          
        <?php $this->load->view('menu_top'); ?>
        <?php $this->load->view('menu_left'); ?>

         <!-- Page Content -->
        <div class="content-wrapper" id="page-wrapper">
            <?php if (!empty($view_content)) $this->load->view($view_content,isset($content) ? $content : ''); ?>
        </div>
    </div>
    <?php } ?>    
    <nav class="navbar navbar-default navbar-fixed-bottom" style="min-height: auto; z-index: auto;">
        <div class="container-fluid">
            <p class="text-muted pull-right">Copyright &copy; <?=date('Y')?> - mCoin</p>
        </div>
    
    <!-- Modal Profile -->
    <div class="modal fade" id="dataProfileModal" tabindex="-1" role="dialog" aria-labelledby="labelDataProfile">
        <div class="modal-dialog" role="document" style="width: 800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <!--button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                    <h4 class="modal-title"><span id="labelDataProfile"></span> User Profile <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/loading.gif')?>" /></span></h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <span id="dataProfileModalURL" style="display: none;"></span>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary" id="simpanProfileModal" data-profile-id="">Simpan</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Modal Notifikasi -->
    <div class="modal fade" id="notifikasiProfileModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiUser">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="labelNotifikasiProfile">Notifikasi</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <span id="statusNotifikasiProfile" style="display: none;"></span>
                    <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiProfileOK">OK</button>
                </div>
            </div>
        </div>
    </div>    
    
    <!-- Modal Password -->
    <div class="modal fade" id="dataPasswordModal" tabindex="-1" role="dialog" aria-labelledby="labelDataPassword">
        <div class="modal-dialog" role="document" style="width: 800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <!--button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                    <h4 class="modal-title"><span id="labelDataPassword"></span> User Password <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/loading.gif')?>" /></span></h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <span id="dataPasswordModalURL" style="display: none;"></span>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary" id="simpanPasswordModal" data-profile-id="">Simpan</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Modal Notifikasi Password -->
    <div class="modal fade" id="notifikasiPasswordModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiUser">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="labelNotifikasiPassword">Notifikasi</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <span id="statusNotifikasiPassword" style="display: none;"></span>
                    <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiPasswordOK">OK</button>
                </div>
            </div>
        </div>
    </div>
    
</body>

</html>

<script>

    // Home Profile
    $('#user-profile').click(function() {
        var url = '<?=site_url('user_management/profile')?>';
        $('#labelDataProfile').text('');
        $('#dataProfileModal .modal-body').html('');
        $('#dataProfileModal .modal-body').load(url);
        $('#dataProfileModalURL').text(url);
    });
    $('#simpanProfileModal').click(function(){
        if ( $('#name_profile').val()=='' ){
            $('#name_profile').focus();
            return false;
        }
        if ( $('#no_identitas_profile').val()=='' ){
            $('#no_identitas_profile').focus();
            return false;
        }
        if ( !$.isNumeric( $('#no_identitas_profile').val() ) ){
            showNotifikasi('notifikasiProfileModal','No Identitas harus angka !');
            $('#no_identitas_profile').focus();
            return false;
        }
        if ( $('#phone_profile').val()=='' ){
            $('#phone_profile').focus();
            return false;
        }
        if ( !$.isNumeric( $('#phone_profile').val() ) ){
            showNotifikasi('notifikasiProfileModal','No Telepon harus angka !');
            $('#phone_profile').focus();
            return false;
        }
        if ( $('#email_profile').val()=='' ){
            $('#email_profile').focus();
            return false;
        }
        
        if ( $('#username_profile').val()=='' ){
            $('#username_profile').focus();
            return false;
        }
        var url = $('#dataProfileModalURL').text();        
        $(this).attr('data-loading-text','Proses Simpan...').button('loading');         
        $.post(url,$('#form-profile').serialize(),
        function(result){
            var result = eval('('+result+')');
            if ( result.success ) $('#dataProfileModal').modal('hide');  
            showNotifikasi('notifikasiProfileModal',result.Msg); 
            $('#statusNotifikasiProfile').val(result.success);
        });   
    });
    $("#buttonNotifikasiProfileOK").click(function() {
        if ( $('#statusNotifikasiProfile').val() ) {
            window.location.href = '<?=site_url('home')?>';
        } else {
            $('#simpanProfileModal').button('reset'); 
        }
	});        
    // End Home Profile
    
    // Home Password
    $('#user-password').click(function() {
        var url = '<?=site_url('user_management/password')?>';
        $('#labelDataPassword').text('');
        $('#dataPasswordModal .modal-body').html('');
        $('#dataPasswordModal .modal-body').load(url);
        $('#dataPasswordModalURL').text(url);
    });
    $('#simpanPasswordModal').click(function(){        
        if ( $('#password_old').val()=='' ){
            $('#password_old').focus();
            return false;
        }
        if ( $('#password_new').val()=='' ){
            $('#password_new').focus();
            return false;
        } 
        if ( $('#password_konfirm').val()=='' ){
            $('#password_konfirm').focus();
            return false;
        }
        if ( $('#password_konfirm').val()!=$('#password_new').val() ){
            showNotifikasi('notifikasiPasswordModal','Password dan Konfirmasi Password tidak sama !');
            $('#password_konfirm').focus();
            return false;
        }
        var url = $('#dataPasswordModalURL').text();        
        $(this).attr('data-loading-text','Proses Simpan...').button('loading');         
        $.post(url,$('#form-password').serialize(),
        function(result){
            var result = eval('('+result+')');
            if ( result.success ) $('#dataPasswordModal').modal('hide');  
            showNotifikasi('notifikasiPasswordModal',result.Msg); 
            $('#statusNotifikasiPassword').val(result.success);
        });   
    });
    $("#buttonNotifikasiPasswordOK").click(function() {
        if ( $('#statusNotifikasiPassword').val() ) {
            window.location.href = '<?=site_url('logout')?>';
        } else {
            $('#simpanPasswordModal').button('reset'); 
        }
	});        
    // End Home Password    
    
    function showNotifikasi(idModal,pesan){
        $('#'+idModal+' .modal-body').html(pesan);
        $('#'+idModal).modal('show');
    }
    
   $(document).ajaxStart(function(){
        $("#loading-data").show();
    }).ajaxStop(function(){
        $("#loading-data").fadeOut("fast");
    $(window).resize();        
    }); 

</script>