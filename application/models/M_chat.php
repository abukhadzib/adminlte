<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_chat extends CI_Model {
    
    function __construct() {
		parent::__construct();
	}
 
    function getChat($id='',$myId='',$limit=15,$page=0,$excel=false)
    {    
        $start = $page>0 ? $limit*$page : 0; 
        
        $sql = "SELECT *,(SELECT name FROM user WHERE id=comments.user_id) AS 'name' FROM comments  WHERE user_id IN ('$id','$myId') ORDER BY id ASC";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
         
        if ($excel){
            return $data;
        } else {        
            $sql  .= " LIMIT ".$start.",".$limit;    
            $data  = $this->db->query($sql);             
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }   
    }

    function getUserOnline($limit=15,$page=0,$filter=array(),$excel=false)
    {        
        $start = $page>0 ? $limit*$page : 0; 
        
        $sql = "SELECT * from user";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
        
        if ($excel){
            return $data;
        } else {        
            $sql  .= " LIMIT ".$start.",".$limit;    
            $data  = $this->db->query($sql);             
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }
        
    }
    
    function addComment($data=array()){
        return $this->db->insert('comments', $data); 
    }
}