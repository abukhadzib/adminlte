<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_pesan extends CI_Model {
    
    function __construct() {
		parent::__construct();
	}
 
    function getPesan($limit=15,$page=0,$filter=array(),$excel=false)
    {        
        $start  = $page>0 ? $limit*$page : 0; 
        
        $where      = "where status='0'";
        $date1      = $this->session->userdata('date1_view_pesan_mesinpesan');
        $date2      = $this->session->userdata('date2_view_pesan_mesinpesan');
        $msisdn     = $this->session->userdata('msisdn_view_pesan_mesinpesan');
        $partner    = $this->session->userdata('partner_view_pesan_mesinpesan');
        $program    = $this->session->userdata('program_view_pesan_mesinpesan');
        $subprogram = $this->session->userdata('subprogram_view_pesan_mesinpesan');
        $pesan      = $this->session->userdata('pesan_view_pesan_mesinpesan');
        
        $partner_user_login = $this->partner;
        $program_user_login = $this->program;
        
        $where     .= !empty($date1) && !empty($date2) ? (empty($where)?' where ':' and ') . "created_date BETWEEN STR_TO_DATE('$date1', '%d-%m-%Y')  AND STR_TO_DATE('$date2 23:59:59', '%d-%m-%Y %H:%i:%s')" : '';
        $where     .= !empty($msisdn)     ? (empty($where)?' where ':' and ') . " msisdn like '%$msisdn%'"  : '';
        $where     .= !empty($partner)    ? (empty($where)?' where ':' and ') . " partner_id='$partner'"    : '';
        $where     .= !empty($program)    ? (empty($where)?' where ':' and ') . " program_id='$program'"    : '';
        $where     .= !empty($subprogram) ? (empty($where)?' where ':' and ') . " subprogram_id='$subprogram'" : '';
        $where     .= !empty($pesan)      ? (empty($where)?' where ':' and ') . " pesan like '%$pesan%'" : '';
        
        $where     .= !empty($partner_user_login) ? (empty($where)?' where ':' and ') . " partner_id='$partner_user_login'"     : '';
        $where     .= !empty($program_user_login) ? (empty($where)?' where ':' and ') . " program_id in ($program_user_login)"  : '';
        
        $sql = "SELECT * from pesan $where order by id desc";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
        
        if ($excel){
            return $data;
        } else {        
            $sql  .= " LIMIT ".$start.",".$limit;    
            $data  = $this->db->query($sql);             
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }        
    }
 
    function getPesanById($id)
    {
        $sql = "SELECT * from pesan where id='$id'";
        $data  = $this->db->query($sql);
        return $data->row();        
    }
 
    function getPesanByTrxId($id)
    {
        $sql = "SELECT * from pesan where trxid='$id'";
        $data  = $this->db->query($sql);
        return $data->row();        
    }
 
    function getPesanByPartnerId($id,$limit=15,$page=0,$tgl='')
    {
        $start = $page>0 ? $limit*$page : 0; 
        $limit = " LIMIT $start,$limit";
        $where = "where status='0' and partner_id='$id'" . (!empty($tgl) ? " and  DATE_FORMAT(created_date,'%d-%m-%Y')='$tgl'" : '');
        $sql   = "SELECT count(*) 'total' from pesan $where";
        $data  = $this->db->query($sql);
        $row   = $data->row();
        $total = $row->total;
        
        $sql   = "SELECT msisdn,trim(CONCAT(partner_keyword,' ',program_keyword,' ',subprogram_keyword)) 'keyword',pesan,pesan_filter,created_date from pesan $where order by created_date desc $limit";    
        $data  = $this->db->query($sql);             
        return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';        
    }
 
    function getPesanByProgramId($id,$limit=15,$page=0,$tgl='')
    {
        $start = $page>0 ? $limit*$page : 0; 
        $limit = " LIMIT $start,$limit";
        $where = "where status='0' and program_id='$id'" . (!empty($tgl) ? " and  DATE_FORMAT(created_date,'%d-%m-%Y')='$tgl'" : '');
        $sql   = "SELECT count(*) 'total' from pesan $where";
        $data  = $this->db->query($sql);
        $row   = $data->row();
        $total = $row->total;
        
        $sql   = "SELECT msisdn,trim(CONCAT(partner_keyword,' ',program_keyword,' ',subprogram_keyword)) 'keyword',pesan,pesan_filter,created_date from pesan $where order by created_date desc $limit";    
        $data  = $this->db->query($sql);             
        return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';        
    }
 
    function getPesanBySubProgramId($id,$limit=15,$page=0,$tgl='')
    {
        $start = $page>0 ? $limit*$page : 0; 
        $limit = " LIMIT $start,$limit";
        $where = "where status='0' and subprogram_id='$id'" . (!empty($tgl) ? " and  DATE_FORMAT(created_date,'%d-%m-%Y')='$tgl'" : '');
        $sql   = "SELECT count(*) 'total' from pesan $where";
        $data  = $this->db->query($sql);
        $row   = $data->row();
        $total = $row->total;
        
        $sql   = "SELECT msisdn,trim(CONCAT(partner_keyword,' ',program_keyword,' ',subprogram_keyword)) 'keyword',pesan,pesan_filter,created_date from pesan $where order by created_date desc $limit";    
        $data  = $this->db->query($sql);             
        return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';        
    }
 
    function getPesans()
    {
        $where      = "where status='0'";
        $date1      = $this->session->userdata('date1_view_pesan_mesinpesan');
        $date2      = $this->session->userdata('date2_view_pesan_mesinpesan');
        $msisdn     = $this->session->userdata('msisdn_view_pesan_mesinpesan');
        $partner    = $this->session->userdata('partner_view_pesan_mesinpesan');
        $program    = $this->session->userdata('program_view_pesan_mesinpesan');
        $subprogram = $this->session->userdata('subprogram_view_pesan_mesinpesan');
        $pesan      = $this->session->userdata('pesan_view_pesan_mesinpesan');
        
        $partner_user_login = $this->partner;
        $program_user_login = $this->program;
        
        $where     .= !empty($date1) && !empty($date2) ? (empty($where)?' where ':' and ') . "created_date BETWEEN STR_TO_DATE('$date1', '%d-%m-%Y')  AND STR_TO_DATE('$date2 23:59:59', '%d-%m-%Y %H:%i:%s')" : '';
        $where     .= !empty($msisdn)     ? (empty($where)?' where ':' and ') . " msisdn like '%$msisdn%'"  : '';
        $where     .= !empty($partner)    ? (empty($where)?' where ':' and ') . " partner_id='$partner'"    : '';
        $where     .= !empty($program)    ? (empty($where)?' where ':' and ') . " program_id='$program'"    : '';
        $where     .= !empty($subprogram) ? (empty($where)?' where ':' and ') . " subprogram_id='$program'" : '';
        $where     .= !empty($pesan)      ? (empty($where)?' where ':' and ') . " pesan like '%$pesan%'" : '';
        
        $where     .= !empty($partner_user_login) ? (empty($where)?' where ':' and ') . " partner_id='$partner_user_login'" : '';
        $where     .= !empty($program_user_login) ? (empty($where)?' where ':' and ') . " program_id in ($program_user_login)" : '';
        
        $sql = "SELECT * from pesan $where order by id desc";
        $data  = $this->db->query($sql);
        return $data->result();
    }
 
    function getPesansCount()
    {
        $where      = "where status='0'";
        $date1      = $this->session->userdata('date1_view_pesan_mesinpesan');
        $date2      = $this->session->userdata('date2_view_pesan_mesinpesan');
        $msisdn     = $this->session->userdata('msisdn_view_pesan_mesinpesan');
        $partner    = $this->session->userdata('partner_view_pesan_mesinpesan');
        $program    = $this->session->userdata('program_view_pesan_mesinpesan');
        $subprogram = $this->session->userdata('subprogram_view_pesan_mesinpesan');
        $pesan      = $this->session->userdata('pesan_view_pesan_mesinpesan');
        
        $partner_user_login = $this->partner;
        $program_user_login = $this->program;
        
        $where     .= !empty($date1) && !empty($date2) ? (empty($where)?' where ':' and ') . "created_date BETWEEN STR_TO_DATE('$date1', '%d-%m-%Y')  AND STR_TO_DATE('$date2 23:59:59', '%d-%m-%Y %H:%i:%s')" : '';
        $where     .= !empty($msisdn)     ? (empty($where)?' where ':' and ') . " msisdn like '%$msisdn%'"  : '';
        $where     .= !empty($partner)    ? (empty($where)?' where ':' and ') . " partner_id='$partner'"    : '';
        $where     .= !empty($program)    ? (empty($where)?' where ':' and ') . " program_id='$program'"    : '';
        $where     .= !empty($subprogram) ? (empty($where)?' where ':' and ') . " subprogram_id='$program'" : '';
        $where     .= !empty($pesan)      ? (empty($where)?' where ':' and ') . " pesan like '%$pesan%'" : '';
        
        $where     .= !empty($partner_user_login) ? (empty($where)?' where ':' and ') . " partner_id='$partner_user_login'" : '';
        $where     .= !empty($program_user_login) ? (empty($where)?' where ':' and ') . " program_id in ($program_user_login)" : '';
        
        $sql     = "SELECT count(*) 'total' from pesan $where";
         
        $data    = $this->db->query($sql);
        $row     = $data->row();
        return '{"total":"'.$row->total.'"}';
    }
	
    function addPesan($data)
    {
        $this->db->insert('pesan_original', $data);
        return $this->db->insert('pesan', $data); 
    }
	
    function addMO($data)
    {
        return $this->db->insert('mo', $data); 
    }
	
    function addDR($data)
    {
        return $this->db->insert('dr', $data); 
    }
	
    function updatePesan($data,$id)
    {
        return $this->db->update('pesan', $data, array('id' => $id)); 
    }
	
    function updatePesanByTrxId($data,$id)
    {
        return $this->db->update('pesan', $data, array('trxid' => $id)); 
    }
 
    function deletePesan($id)
    {
        return $this->db->delete('pesan', array('id'=>$id));         
    }
    	   
}