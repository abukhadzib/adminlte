<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'controllers/Base.php');

class Data_master extends Base 
{
    function __construct()
    {
		parent::__construct();
        $this->load->model('m_data_master'); 
	}
	
	function index()
	{
        if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
         
        redirect(site_url('home'));
	} 
	
	function partner($page=1)
	{
        if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
        
        $page = is_numeric($page) ? $page : 1;
        $page = $page < 1 ? 1 : $page;
        $page--;
        
        if ($this->input->post())
        {   
            $search = $this->input->post('search');
            $setSearchPartner = array('search_partner_mesinpesan' => $search);
			$this->session->set_userdata($setSearchPartner);
            redirect(site_url('data_master/partner'));
        }    
                
        $data = $this->m_data_master->getPartner($this->limit_page,$page);
        $data = json_decode($data);
        
        $this->data['page']         = $page;
        $this->data['perpage']      = $this->limit_page;
        $this->data['username']     = $this->username;
        $this->data['name']         = $this->name;
        $this->data['search']       = $this->session->userdata('search_partner_mesinpesan');        
        $this->data['content']      = $data->rows;
        $this->data['view_content'] = 'data_master/partner';
        
        $link = site_url('data_master/partner');
        $this->data['pagination'] = paging(ceil($data->total/$this->limit_page),$page+1,$link,$data->total,$this->limit_page);
        
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'data_master/partner','data'=>'');
        
        if ( !$this->cekAksesAction('data_master/partner','read_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';    
        }
        
        $this->addLog($data_log);
        
        $this->load->view('home',$this->data);
	} 
	
	function partner_add()
	{
        if ( empty($this->username) ) {
            echo "Session anda telah berakhir, refresh browser anda.";
            exit;
	    } 
        
        if ($this->input->post())
        {
            if ( !$this->cekAksesAction('data_master/partner','create_') ){
                echo "{success:false, Msg:'Anda tidak mempunyai hak menambah data Partner !.'}";
                exit;
            }  
			
            $keyword = $this->input->post('keyword');
            
            if ( $this->m_data_master->cekPartnerKeywordExist($keyword) ){
                echo "{success:false, Msg:'Keyword Partner telah digunakan !.'}";
                exit;
            }
            
            $data = array( 'name'         => $this->input->post('name'),
                           'email'        => $this->input->post('email'),
                           'phone'        => $this->input->post('phone'),
                           'address1'     => $this->input->post('address1'),
                           'address2'     => $this->input->post('address2'),
                           'npwp'         => $this->input->post('npwp'),
                           'zip'          => $this->input->post('zipcode'),
                           'city'         => $this->input->post('city'),
                           'country'      => $this->input->post('country'),
                           'keyword'      => $this->input->post('keyword'),
                           'description'  => $this->input->post('description'),
                           'sms_response' => $this->input->post('sms_response'),
                           'secure_key'   => unique_id()
                    );
                    
            $data_log = array('userid'=>$this->userid,'action'=>'Add','modul'=>'data_master/partner_add','data'=>json_encode($data));
            $this->addLog($data_log);
            
            if ( $this->m_data_master->addPartner($data))
            {
                echo "{success:true, Msg:'Proses tambah data Partner <b>Berhasil</b>'}";
                exit; 
            }    
            else
            {
                echo "{success:true, Msg:'Ada kesalahan dalam menambah data Partner.'}";
                exit; 
            }        
        }
        
        $this->data['content']      = '';
        
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'data_master/partner_add','data'=>'');
        
        if ( !$this->cekAksesAction('data_master/partner','create_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';    
        }
        
        $this->addLog($data_log);
                
        $this->load->view('data_master/partner_add',$this->data);
	} 
	
	function partner_edit($id='')
	{
        if ( empty($this->username) ) {
            echo "Session anda telah berakhir, refresh browser anda.";
            exit;
	    } 
        
        if ( empty($id) ) {
            echo "{success:false, Msg:'Parameter tidak lengkap !.'}";
            exit;
	    } 
        
        if ($this->input->post())
        {
            if ( !$this->cekAksesAction('data_master/partner','update_') ){
                echo "{success:false, Msg:'Anda tidak mempunyai hak merubah data Partner !.'}";
                exit;
            }
			
            $keyword = $this->input->post('keyword');
            
            if ( $this->m_data_master->cekPartnerKeywordExist($keyword,$this->input->post('partner_id')) ){
                echo "{success:false, Msg:'Keyword Partner telah digunakan !.'}";
                exit;
            }
            
            $data = array( 'name'          => $this->input->post('name'),
                           'phone'         => $this->input->post('phone'),
                           'address1'      => $this->input->post('address1'),
                           'address2'      => $this->input->post('address2'),
                           'npwp'          => $this->input->post('npwp'),
                           'zip'           => $this->input->post('zipcode'),
                           'city'          => $this->input->post('city'),
                           'country'       => $this->input->post('country'),
                           'keyword'       => $this->input->post('keyword'),
                           'description'   => $this->input->post('description'),
                           'sms_response'  => $this->input->post('sms_response'),
                           'modified_date' => date('Y-m-d H:i:s'),
                           'secure_key'    => unique_id()
                    );
            
            $data_log = array('userid'=>$this->userid,'action'=>'Update','modul'=>'data_master/partner_edit/'.$id,'data'=>json_encode($data));
            $this->addLog($data_log);
            
            if ( $this->m_data_master->updatePartner($data,$this->input->post('partner_id')))
            {
                echo "{success:true, Msg:'Proses rubah data Partner <b>Berhasil</b>'}";
                exit; 
            }    
            else
            {
                echo "{success:true, Msg:'Ada kesalahan dalam merubah data Partner.'}";
                exit; 
            }        
        }
        
        $this->data['partner']  = $this->m_data_master->getPartnerById($id);
        
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'data_master/partner_edit/'.$id,'data'=>'');
        
        if ( !$this->cekAksesAction('data_master/partner','update_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';    
        }
        
        $this->addLog($data_log);
                
        $this->load->view('data_master/partner_edit',$this->data);
	}  
	
	function partner_delete($id='')
	{
        if ( empty($this->username) ) {
            echo "Session anda telah berakhir, refresh browser anda.";
            exit;
	    } 
        
        if ( empty($id) ) {
            echo "{success:false, Msg:'Parameter tidak lengkap !'}";
            exit;
	    } 
        
        if ($this->input->post() && $this->input->post('action')=='delete')
        {
            $id_post = $this->input->post('id');
            
            $data_log = array('userid'=>$this->userid,'action'=>'Delete','modul'=>'data_master/partner_delete/'.$id,'data'=>'');
            
            if ( $id_post!=$id ){
                echo "{success:false, Msg:'Parameter delete salah !'}";
                exit;
            }
            
            if ( !$this->cekAksesAction('data_master/partner','delete_') ){
                echo "{success:false, Msg:'Anda tidak berhak menghapus data Partner !'}";
                $data_log['data'] = "{success:false, Msg:'You have no rights to delete partner !'}";
                $this->addLog($data_log);
                exit; 
            }
            
            if ( $this->m_data_master->cekPartnerExist($id) ){
                echo "{success:false, Msg:'Data Partner masih digunakan !.'}";
                exit;
            }
            
            if ( $this->m_data_master->deletePartner($id) ){
                echo "{success:true, Msg:'Proses hapus data Partner <b>Berhasil</b> !'}";
                $data_log['data'] = "{success:true, Msg:'Berhasil hapus partner'}";
                $this->addLog($data_log);
            }
        } else {
            echo "{success:false, Msg:'Parameter hapus data Partner salah !'}";
        }
	} 
	
	function program($page=1)
	{
        if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
         
        $page = is_numeric($page) ? $page : 1;
        $page = $page < 1 ? 1 : $page;
        $page--;
                
        if ($this->input->post())
        {   
            $setSubProgramFilter = array('name_view_program_mesinpesan'        => $this->input->post('search_name'),
                                         'description_view_program_mesinpesan' => $this->input->post('search_description'),
                                         'keyword_view_program_mesinpesan'     => $this->input->post('search_keyword'),
                                         'partner_view_program_mesinpesan'     => $this->input->post('search_partner'),
                                         'program_view_program_mesinpesan'     => $this->input->post('search_program'),
                                         'response_view_program_mesinpesan'    => $this->input->post('search_response'));
			$this->session->set_userdata($setSubProgramFilter);
            redirect(site_url('data_master/program'));
        }    
                
        $data = $this->m_data_master->getProgram($this->limit_page,$page);
        $data = json_decode($data);
        
        $this->data['page'] = $page;
        $this->data['perpage'] = $this->limit_page;
        $this->data['username']     = $this->username;
        $this->data['name']         = $this->name;
        $this->data['content']      = $data->rows;
        $this->data['view_content'] = 'data_master/program';
        
        $this->data['name_view_program_mesinpesan']        = $this->session->userdata('name_view_program_mesinpesan');
        $this->data['description_view_program_mesinpesan'] = $this->session->userdata('description_view_program_mesinpesan');
        $this->data['keyword_view_program_mesinpesan']     = $this->session->userdata('keyword_view_program_mesinpesan');
        $this->data['partner_view_program_mesinpesan']     = $this->session->userdata('partner_view_program_mesinpesan');
        $this->data['program_view_program_mesinpesan']     = $this->session->userdata('program_view_program_mesinpesan');
        $this->data['response_view_program_mesinpesan']    = $this->session->userdata('response_view_program_mesinpesan');
        
        $this->data['partner']      = $this->m_data_master->getPartners();                 
        
        $link = site_url('data_master/program');
        $this->data['pagination'] = paging(ceil($data->total/$this->limit_page),$page+1,$link,$data->total,$this->limit_page);
        
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'data_master/program','data'=>'');
        
        if ( !$this->cekAksesAction('data_master/program','read_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';           
        }
        
        $this->addLog($data_log);
                
        $this->load->view('home',$this->data);
	} 
    
    function program_clear_search()
    {
        $setProgramFilter = array('name_view_program_mesinpesan'        => '',
                                  'description_view_program_mesinpesan' => '',
                                  'keyword_view_program_mesinpesan'     => '',
                                  'partner_view_program_mesinpesan'     => '',
                                  'program_view_program_mesinpesan'     => '',
                                  'response_view_program_mesinpesan'    => '');
		$this->session->set_userdata($setProgramFilter);
        redirect(site_url('data_master/program'));    
    }
	
	function program_add()
	{
        if ( empty($this->username) ) {
            echo "Session anda telah berakhir, refresh browser anda.";
            exit;
	    } 
        
        if ($this->input->post())
        {    
            if ( !$this->cekAksesAction('data_master/program','create_') ){
                echo "{success:false, Msg:'Anda tidak mempunyai hak menambah data Program !.'}";
                exit;
            }
            
            $program_name = $this->input->post('name');
            $description  = $this->input->post('description');
            $keyword      = $this->input->post('keyword');
            $partner      = $this->input->post('partner');
            $sms_response = $this->input->post('sms_response');
            
            if ( $this->m_data_master->cekProgramKeywordExist($keyword,$partner) ){
                echo "{success:false, Msg:'Keyword Program telah digunakan !.'}";
                exit;
            }            
            
            $data  = array('name'         => $program_name,
                           'description'  => $description,
                           'keyword'      => $keyword,
                           'partner_id'   => $partner,
                           'sms_response' => $sms_response,
                           'secure_key'   => unique_id());
            
            if ( $this->m_data_master->addProgram($data) )
            {
                $program_id = $this->db->insert_id();
                
                $data_log = array('userid'=>$this->userid,'action'=>'Add','modul'=>'data_master/program_add','data'=>json_encode(array($data)));
                $this->addLog($data_log);
                
                echo "{success:true, Msg:'Proses tambah data Program <b>Berhasil</b>'}";
                exit; 
            } 
            else
            {
                echo "{success:false, Msg:'Proses tambah data Program <b>Gagal</b>'}";
                exit; 
            }            
        }
        
        $this->data['partner'] = $this->m_data_master->getPartners();         
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'data_master/program_add','data'=>'');
        
        if ( !$this->cekAksesAction('data_master/program','create_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';      
        }
        
        $this->addLog($data_log);
                
        $this->load->view('data_master/program_add',$this->data);
	} 
	
	function program_edit($id='')
	{
        if ( empty($this->username) ) {
            echo "Session anda telah berakhir, refresh browser anda.";
            exit;
	    } 
        
        if ( empty($id) ) {
            echo "{success:false, Msg:'Parameter tidak lengkap !.'}";
            exit;
	    } 
        
        if ($this->input->post())
        {
            if ( !$this->cekAksesAction('data_master/program','update_') ){
                echo "{success:false, Msg:'Anda tidak mempunyai hak merubah data Program !.'}";
                exit;
            }
            
            $program_id   = $this->input->post('program_id');
            $program_name = $this->input->post('name');
            $keyword      = $this->input->post('keyword');
            $description  = $this->input->post('description');
            $partner      = $this->input->post('partner');
            $sms_response = $this->input->post('sms_response');
            
            if ( $this->m_data_master->cekProgramKeywordExist($keyword,$partner,$program_id) ){
                echo "{success:false, Msg:'Keyword Program telah digunakan !.'}";
                exit;
            }            
            
            $data  = array('name'          => $program_name,
                           'description'   => $description,
                           'keyword'       => $keyword,
                           'partner_id'    => $partner,
                           'sms_response'  => $sms_response,
                           'modified_date' => date('Y-m-d H:i:s'),
                           'secure_key'    => unique_id());
            
            if ( $this->m_data_master->updateProgram($data,$program_id) )
            {
                $data_log = array('userid'=>$this->userid,'action'=>'Update','modul'=>'data_master/program_edit/'.$id,'data'=>json_encode(array($data)));
                $this->addLog($data_log);
                
                echo "{success:true, Msg:'Proses rubah data Program <b>Berhasil</b>'}";
                exit; 
            }
            else
            {
                echo "{success:false, Msg:'Proses rubah data Program <b>Gagal</b>'}";
                exit; 
            }
        }
        
        $this->data['partner'] = $this->m_data_master->getPartners();         
        $this->data['program'] = $this->m_data_master->getProgramById($id);
        
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'data_master/program_edit/'.$id,'data'=>'');
        
        if ( !$this->cekAksesAction('data_master/program','update_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';    
        }
        
        $this->addLog($data_log);
                
        $this->load->view('data_master/program_edit',$this->data);
	}
	
	function program_delete($id='')
	{
        if ( empty($this->username) ) {
            echo "{success:false, Msg:'Login dibutuhkan !'}";
            exit;
	    } 
        
        if ( empty($id) ) {
            echo "{success:false, Msg:'Parameter tidak lengkap !'}";
            exit;
	    } 
        
        if ($this->input->post() && $this->input->post('action')=='delete')
        {
            $id_post = $this->input->post('id');
            
            if ( $id_post!=$id ){
                echo "{success:false, Msg:'Parameter delete salah !'}";
                exit;
            }
            
            if ( !$this->cekAksesAction('data_master/program','delete_') ){
                echo "{success:false, Msg:'Anda tidak berhak menghapus data Program !'}";
                exit; 
            }
            
            if ( $this->m_data_master->cekProgramExist($id) ){
                echo "{success:false, Msg:'Data Program masih digunakan !.'}";
                exit;
            }
            
            if ( $this->m_data_master->deleteProgram($id_post) ){
                $data_log = array('userid'=>$this->userid,'action'=>'Delete','modul'=>'data_master/program_delete/'.$id,'data'=>'');
                $this->addLog($data_log);
                echo "{success:true, Msg:'Proses hapus data program <b>Berhasil</b>'}";
            }
        } else {
            echo "{success:false, Msg:'Parameter hapus data Program salah !'}";
        }
	} 
	
	function subprogram($page=1)
	{
        if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
        
        $page = is_numeric($page) ? $page : 1;
        $page = $page < 1 ? 1 : $page;
        $page--;
                
        if ($this->input->post())
        {   
            $setSubProgramFilter = array('name_view_subprogram_mesinpesan'        => $this->input->post('search_name'),
                                         'description_view_subprogram_mesinpesan' => $this->input->post('search_description'),
                                         'keyword_view_subprogram_mesinpesan'     => $this->input->post('search_keyword'),
                                         'partner_view_subprogram_mesinpesan'     => $this->input->post('search_partner'),
                                         'program_view_subprogram_mesinpesan'     => $this->input->post('search_program'),
                                         'response_view_subprogram_mesinpesan'    => $this->input->post('search_response'));
			$this->session->set_userdata($setSubProgramFilter);
            redirect(site_url('data_master/subprogram'));
        }    
                
        $data = $this->m_data_master->getSubProgram($this->limit_page,$page);
        $data = json_decode($data);
        
        $this->data['page']         = $page;
        $this->data['perpage']      = $this->limit_page;
        $this->data['username']     = $this->username;
        $this->data['name']         = $this->name;
        $this->data['content']      = $data->rows;
        $this->data['view_content'] = 'data_master/subprogram';
        
        $this->data['name_view_subprogram_mesinpesan']        = $this->session->userdata('name_view_subprogram_mesinpesan');
        $this->data['description_view_subprogram_mesinpesan'] = $this->session->userdata('description_view_subprogram_mesinpesan');
        $this->data['keyword_view_subprogram_mesinpesan']     = $this->session->userdata('keyword_view_subprogram_mesinpesan');
        $this->data['partner_view_subprogram_mesinpesan']     = $this->session->userdata('partner_view_subprogram_mesinpesan');
        $this->data['program_view_subprogram_mesinpesan']     = $this->session->userdata('program_view_subprogram_mesinpesan');
        $this->data['response_view_subprogram_mesinpesan']    = $this->session->userdata('response_view_subprogram_mesinpesan');
        
        $this->data['partner']      = $this->m_data_master->getPartners();                 
        $this->data['program']      = $this->m_data_master->getPrograms($this->session->userdata('partner_view_subprogram_mesinpesan'));
                
        $link = site_url('data_master/subprogram');
        $this->data['pagination'] = paging(ceil($data->total/$this->limit_page),$page+1,$link,$data->total,$this->limit_page);
        
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'data_master/subprogram','data'=>'');
        
        if ( !$this->cekAksesAction('data_master/subprogram','read_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';           
        }
        
        $this->addLog($data_log);
                
        $this->load->view('home',$this->data);
	} 
    
    function subprogram_clear_search()
    {
        $setSubProgramFilter = array('name_view_subprogram_mesinpesan'        => '',
                                     'description_view_subprogram_mesinpesan' => '',
                                     'keyword_view_subprogram_mesinpesan'     => '',
                                     'partner_view_subprogram_mesinpesan'     => '',
                                     'program_view_subprogram_mesinpesan'     => '',
                                     'response_view_subprogram_mesinpesan'    => '');
		$this->session->set_userdata($setSubProgramFilter);
        redirect(site_url('data_master/subprogram'));    
    }
	
	function subprogram_add()
	{
        if ( empty($this->username) ) {
            echo "Session anda telah berakhir, refresh browser anda.";
            exit;
	    } 
        
        if ($this->input->post())
        {    
            if ( !$this->cekAksesAction('data_master/subprogram','create_') ){
                echo "{success:false, Msg:'Anda tidak mempunyai hak menambah data SubProgram !.'}";
                exit;
            }
            
            $subprogram_name = $this->input->post('name');
            $description     = $this->input->post('description');
            $keyword         = $this->input->post('keyword');
            $partner         = $this->input->post('partner');
            $program         = $this->input->post('program');
            $sms_response    = $this->input->post('sms_response');
            
            if ( $this->m_data_master->cekSubProgramKeywordExist($keyword,$partner,$program) ){
                echo "{success:false, Msg:'Keyword Sub Program telah digunakan !.'}";
                exit;
            }            
            
            $data  = array('name'         => $subprogram_name,
                           'description'  => $description,
                           'keyword'      => $keyword,
                           'partner_id'   => $partner,
                           'program_id'   => $program,
                           'sms_response' => $sms_response,
                           'secure_key'   => unique_id());
            
            if ( $this->m_data_master->addSubProgram($data) )
            {
                $subprogram_id = $this->db->insert_id();
                
                $data_log = array('userid'=>$this->userid,'action'=>'Add','modul'=>'data_master/subprogram_add','data'=>json_encode(array($data)));
                $this->addLog($data_log);
                
                echo "{success:true, Msg:'Proses tambah data Sub Program <b>Berhasil</b>'}";
                exit; 
            } 
            else
            {
                echo "{success:false, Msg:'Proses tambah data Sub Program <b>Gagal</b>'}";
                exit; 
            }            
        }
        
        $this->data['partner'] = $this->m_data_master->getPartners();         
        $this->data['program'] = '';         
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'data_master/subprogram_add','data'=>'');
        
        if ( !$this->cekAksesAction('data_master/subprogram','create_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';      
        }
        
        $this->addLog($data_log);
                
        $this->load->view('data_master/subprogram_add',$this->data);
	} 
	
	function subprogram_edit($id='')
	{
        if ( empty($this->username) ) {
            echo "Session anda telah berakhir, refresh browser anda.";
            exit;
	    } 
        
        if ( empty($id) ) {
            echo "{success:false, Msg:'Parameter tidak lengkap !.'}";
            exit;
	    } 
        
        if ($this->input->post())
        {
            if ( !$this->cekAksesAction('data_master/subprogram','update_') ){
                echo "{success:false, Msg:'Anda tidak mempunyai hak merubah data Sub Program !.'}";
                exit;
            }
            
            $subprogram_id   = $this->input->post('subprogram_id');
            $subprogram_name = $this->input->post('name');
            $keyword         = $this->input->post('keyword');
            $description     = $this->input->post('description');
            $partner         = $this->input->post('partner');
            $program         = $this->input->post('program');
            $sms_response    = $this->input->post('sms_response');
            
            if ( $this->m_data_master->cekSubProgramKeywordExist($keyword,$partner,$program,$subprogram_id) ){
                echo "{success:false, Msg:'Keyword Sub Program telah digunakan !.'}";
                exit;
            }            
            
            $data  = array('name'          => $subprogram_name,
                           'description'   => $description,
                           'keyword'       => $keyword,
                           'partner_id'    => $partner,
                           'program_id'    => $program,
                           'sms_response'  => $sms_response,
                           'modified_date' => date('Y-m-d H:i:s'),
                           'secure_key'    => unique_id());
            
            if ( $this->m_data_master->updateSubProgram($data,$subprogram_id) )
            {
                $data_log = array('userid'=>$this->userid,'action'=>'Update','modul'=>'data_master/subprogram_edit/'.$id,'data'=>json_encode(array($data)));
                $this->addLog($data_log);
                
                echo "{success:true, Msg:'Proses rubah data Sub Program <b>Berhasil</b>'}";
                exit; 
            }
            else
            {
                echo "{success:false, Msg:'Proses rubah data Sub Program <b>Gagal</b>'}";
                exit; 
            }
        }
        
        $this->data['subprogram'] = $this->m_data_master->getSubProgramById($id);
        $this->data['partner']    = $this->m_data_master->getPartners();         
        $this->data['program']    = $this->m_data_master->getPrograms($this->data['subprogram']->partner_id);         
        
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'data_master/subprogram_edit/'.$id,'data'=>'');
        
        if ( !$this->cekAksesAction('data_master/subprogram','update_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';    
        }
        
        $this->addLog($data_log);
                
        $this->load->view('data_master/subprogram_edit',$this->data);
	}
	
	function subprogram_delete($id='')
	{
        if ( empty($this->username) ) {
            echo "{success:false, Msg:'Login dibutuhkan !'}";
            exit;
	    } 
        
        if ( empty($id) ) {
            echo "{success:false, Msg:'Parameter tidak lengkap !'}";
            exit;
	    } 
        
        if ($this->input->post() && $this->input->post('action')=='delete')
        {
            $id_post = $this->input->post('id');
            
            if ( $id_post!=$id ){
                echo "{success:false, Msg:'Parameter delete salah !'}";
                exit;
            }
            
            if ( !$this->cekAksesAction('data_master/subprogram','delete_') ){
                echo "{success:false, Msg:'Anda tidak berhak menghapus data Sub Program !'}";
                exit; 
            }
            
            if ( $this->m_data_master->deleteSubProgram($id_post) ){
                $data_log = array('userid'=>$this->userid,'action'=>'Delete','modul'=>'data_master/subprogram_delete/'.$id,'data'=>'');
                $this->addLog($data_log);
                echo "{success:true, Msg:'Proses hapus data Sub program <b>Berhasil</b>'}";
            }
        } else {
            echo "{success:false, Msg:'Parameter hapus data Sub Program salah !'}";
        }
	} 
	
	function filter($page=1)
	{
        if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
        
        $page = is_numeric($page) ? $page : 1;
        $page = $page < 1 ? 1 : $page;
        $page--;
        
        if ($this->input->post())
        {   
            $search = $this->input->post('search');
            $setSearchFilter = array('search_filter_mesinpesan' => $search);
			$this->session->set_userdata($setSearchFilter);
            redirect(site_url('data_master/filter'));
        }    
        
        $data = $this->m_data_master->getFilter($this->limit_page,$page);
        $data = json_decode($data);
        
        $this->data['page']         = $page;
        $this->data['perpage']      = $this->limit_page;
        $this->data['username']     = $this->username;
        $this->data['name']         = $this->name;
        $this->data['content']      = $data->rows;
        $this->data['search']       = $this->session->userdata('search_filter_mesinpesan');        
        $this->data['view_content'] = 'data_master/filter';
        
        $link = site_url('data_master/filter');
        $this->data['pagination'] = paging(ceil($data->total/$this->limit_page),$page+1,$link,$data->total,$this->limit_page);
        
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'data_master/filter','data'=>'');
        
        if ( !$this->cekAksesAction('data_master/filter','read_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';           
        }
        
        $this->addLog($data_log);
                
        $this->load->view('home',$this->data);
	} 
	
	function filter_add()
	{
        if ( empty($this->username) ) {
            echo "Session anda telah berakhir, refresh browser anda.";
            exit;
	    } 
        
        if ($this->input->post())
        {    
            if ( !$this->cekAksesAction('data_master/filter','create_') ){
                echo "{success:false, Msg:'Anda tidak mempunyai hak menambah data Filter !.'}";
                exit;
            }
            
            $word = $this->input->post('word');
            
            $data  = array('word'=>$word);
            
            if ( $this->m_data_master->addFilter($data) )
            {
                $filter_id = $this->db->insert_id();
                
                $data_log = array('userid'=>$this->userid,'action'=>'Add','modul'=>'data_master/filter_add','data'=>json_encode(array($data)));
                $this->addLog($data_log);
                
                echo "{success:true, Msg:'Proses tambah data Filter <b>Berhasil</b>'}";
                exit; 
            } 
            else
            {
                echo "{success:false, Msg:'Proses tambah data Filter <b>Gagal</b>'}";
                exit; 
            }            
        }
        
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'data_master/filter_add','data'=>'');
        
        if ( !$this->cekAksesAction('data_master/filter','create_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';      
        }
        
        $this->addLog($data_log);
                
        $this->load->view('data_master/filter_add',$this->data);
	} 
	
	function filter_edit($id='')
	{
        if ( empty($this->username) ) {
            echo "Session anda telah berakhir, refresh browser anda.";
            exit;
	    } 
        
        if ( empty($id) ) {
            echo "{success:false, Msg:'Parameter tidak lengkap !.'}";
            exit;
	    } 
        
        if ($this->input->post())
        {
            if ( !$this->cekAksesAction('data_master/filter','update_') ){
                echo "{success:false, Msg:'Anda tidak mempunyai hak merubah data Filter !.'}";
                exit;
            }
            
            $filter_id = $this->input->post('filter_id');
            $word      = $this->input->post('word');
            
            $data  = array('word'=>$word,'modified_date'=>date('Y-m-d H:i:s'));
            
            if ( $this->m_data_master->updateFilter($data,$filter_id) )
            {
                $data_log = array('userid'=>$this->userid,'action'=>'Update','modul'=>'data_master/filter_edit/'.$id,'data'=>json_encode(array($data)));
                $this->addLog($data_log);
                
                echo "{success:true, Msg:'Proses rubah data Filter <b>Berhasil</b>'".json_encode($data)."}";
                exit; 
            }
            else
            {
                echo "{success:false, Msg:'Proses rubah data Filter <b>Gagal</b>'}";
                exit; 
            }
        }
        
        $this->data['filter'] = $this->m_data_master->getFilterById($id);
        
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'data_master/filter_edit/'.$id,'data'=>'');
        
        if ( !$this->cekAksesAction('data_master/filter','update_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';    
        }
        
        $this->addLog($data_log);
                
        $this->load->view('data_master/filter_edit',$this->data);
	}
	
	function filter_delete($id='')
	{
        if ( empty($this->username) ) {
            echo "{success:false, Msg:'Login dibutuhkan !'}";
            exit;
	    } 
        
        if ( empty($id) ) {
            echo "{success:false, Msg:'Parameter tidak lengkap !'}";
            exit;
	    } 
        
        if ($this->input->post() && $this->input->post('action')=='delete')
        {
            $id_post = $this->input->post('id');
            
            if ( $id_post!=$id ){
                echo "{success:false, Msg:'Parameter delete salah !'}";
                exit;
            }
            
            if ( !$this->cekAksesAction('data_master/filter','delete_') ){
                echo "{success:false, Msg:'Anda tidak berhak menghapus data Filter !'}";
                exit; 
            }
            
            if ( $this->m_data_master->deleteFilter($id_post) ){
                $data_log = array('userid'=>$this->userid,'action'=>'Delete','modul'=>'data_master/filter_delete/'.$id,'data'=>'');
                $this->addLog($data_log);
                echo "{success:true, Msg:'Proses hapus data filter <b>Berhasil</b>'}";
            }
        } else {
            echo "{success:false, Msg:'Parameter hapus data Filter salah !'}";
        }
	}  
    
    function getProgramPartnerData($partner='')
    {
        $program = $this->m_data_master->getProgramPartnerData($partner);     
        echo $program;
    }    
    
    function getSubProgramPartnerData($partner='',$program='')
    {
        $program = $this->m_data_master->getSubProgramPartnerData($partner,$program);     
        echo $program;
    }
}
