<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'controllers/Base.php');

class Logout extends Base {

    function __construct()
    {
		parent::__construct();        
        $this->load->model('m_login'); 
        
	set_cookie('authCookieAdminLTE','',time()-3600);
		
        $this->m_login->updateOnlineStatus($this->session->userdata('userid_AdminLTE'));
        
        $setuser = array('userid_AdminLTE'     => '',
                         'username_AdminLTE'   => '',
                         'name_AdminLTE'       => '',
                         'role_AdminLTE'       => '',
                         'isadmin_AdminLTE'    => '',
                         'partner_AdminLTE'    => '',
                         'program_AdminLTE'    => '',
                         'email_user_AdminLTE' => '');
         
        $data_log = array('userid'=>$this->userid,'action'=>'Logout','modul'=>'Logout','data'=>json_encode($setuser));
        $this->addLog($data_log);
        
        $this->session->set_userdata($setuser);          
                        
        redirect(site_url('login'));         
	}
    
    function index()
	{
        
	}
    
}
