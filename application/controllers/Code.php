<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'controllers/Base.php');

class Code extends Base {

	function __construct()
    {
		parent::__construct(); 
        $this->load->model('m_data_master'); 
        $this->load->model('m_pesan'); 
	}
	
	function index()
	{
		if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
         
        redirect(site_url('home'));
	 
        
        
	} 
        
        function text() {
            $this->load->library('ciqrcode');
	
            //header("Content-Type: image/png");
            $params['data'] = 'This is a text to encode become QR Code';
            $params['trxid'] = date('YmdHis');
            $params['level'] = 'H';
            $params['size'] = 6;
            $date = date('YmdHis');
            $name = 'Text'.$date;
            $params['savename'] = IMGPATH.$name.'.png';
            $this->ciqrcode->generateCode($params);
            
            $this->data['username']     = $this->username;
            $this->data['name']         = $this->name;
            $this->data['view_content'] = 'code';
            $this->data['code'] = $name;
            $this->load->view('home',  $this->data);
        }
        
        function Tel() {
            $this->load->library('ciqrcode');
            //header("Content-Type: image/png");
            $params['level'] = 'H';
            $params['size'] = 5;
            $phoneNo = '085777544678';
    
            // we building raw data
            $params['data'] = 'tel:'.$phoneNo; ; 
            
            $date = date('YmdHis');
            $name = 'Tel'.$date;
            $params['savename'] = IMGPATH.$name.'.png';
            $this->ciqrcode->generateCode($params);
            
            $this->data['username']     = $this->username;
            $this->data['name']         = $this->name;
            $this->data['view_content'] = 'code';
            $this->data['code'] = $name;
            $this->load->view('home',  $this->data);
        }
        
         function Sms() {
            $this->load->library('ciqrcode');
            //header("Content-Type: image/png");
            $params['level'] = 'H';
            $params['size'] = 5;
            $phoneNo = '085777544678';
    
            // we building raw data
            $params['data'] = 'sms:'.$phoneNo; ; 
            
            $date = date('YmdHis');
            $name = 'SMS'.$date;
            $params['savename'] = IMGPATH.$name.'.png';
            $this->ciqrcode->generateCode($params);
            
            $this->data['username']     = $this->username;
            $this->data['name']         = $this->name;
            $this->data['view_content'] = 'code';
            $this->data['code'] = $name;
            $this->load->view('home',  $this->data);
        }
        
        function Skype() {
            $this->load->library('ciqrcode');
            //header("Content-Type: image/png");
            $params['level'] = 'H';
            $params['size'] = 3;
            
            // here our data
            $skypeUserName = 'abukhadzib';

            // we building raw data
            $params['data'] = 'skype:'.urlencode($skypeUserName).'?call'; 
            
            $date = date('YmdHis');
            $name = 'Skype'.$date;
            $params['savename'] = IMGPATH.$name.'.png';
            $this->ciqrcode->generateCode($params);
            
            $this->data['username']     = $this->username;
            $this->data['name']         = $this->name;
            $this->data['view_content'] = 'code';
            $this->data['code'] = $name;
            $this->load->view('home',  $this->data);
        }
        
        function Email() {
            
            $this->load->library('ciqrcode');
           // header("Content-Type: image/png");
            $params['level'] = 'H';
            $params['size'] = 3;
            
            // here our data
            $email = 'syamsul.anwar21@gmail.com';
            $subject = 'question';
            $body = 'please write your question here';

            // we building raw data
            $params['data'] = 'mailto:'.$email.'?subject='.urlencode($subject).'&body='.urlencode($body); 
            
            $date = date('YmdHis');
            $name = 'Email'.$date;
            $params['savename'] = IMGPATH.$name.'.png';
            $this->ciqrcode->generateCode($params);
            
            $this->data['username']     = $this->username;
            $this->data['name']         = $this->name;
            $this->data['view_content'] = 'code';
            $this->data['code'] = $name;
            $this->load->view('home',  $this->data);
             
        }
        function Vcard() {
            $this->load->library('ciqrcode');
	
           // header("Content-Type: image/png");
            $params['data'] = 'This is a text to encode become QR Code';
            $params['trxid'] = date('YmdHis');
            $params['level'] = 'H';
            $params['size'] = 3;
            $name = 'Syamsul';
            $phone = '087782257270';
            $address = 'Jln. Ciputat Raya no 37 Pondok Pinang Jakarta Selatan';
            $kantor = 'McoinAsia';
            $email = 'syamsul.anwar21@gmail.com';
            $url    = 'www.okedong.com';
            $avatarJpegFileName = $this->img.'avatar5.png';
            //BEGIN:VCARD VERSION:3.0 N:User;Test FN:Test User ORG:Example Organisation TITLE:asgfas [asgasg] TEL;TYPE=work,voice:2523626 TEL;TYPE=cell,voice:2365236 TEL;TYPE=work,fax:236236 URL;TYPE=work:www.example.com EMAIL;TYPE=internet,pref:testu@example.com REV:20121015T195243Z END:VCARD
            
            // we building raw data
            $params['data']  = 'BEGIN:VCARD'."\n";
            $params['data'] .= 'FN:'.$name."\n";
            $params['data'] .= 'TEL;CELL;VOICE:'.$phone."\n";
            $params['data'] .= 'ORG:'.$kantor."\n";
            $params['data'] .= 'ADR;TYPE=work:'.$address."\n";
            $params['data'] .= 'EMAIL:'.$email."\n";
            $params['data'] .= 'URL:'.$url."\n";
            //$params['data'] .= 'PHOTO;JPEG;ENCODING=BASE64:'.base64_encode(file_get_contents($avatarJpegFileName))."\n";
            $params['data'] .= 'END:VCARD'; 
            
            $date = date('YmdHis');
            $name = 'Vcard'.$date;
            $params['savename'] = IMGPATH.$name.'.png';
            $this->ciqrcode->generateCode($params);
            
            $this->data['username']     = $this->username;
            $this->data['name']         = $this->name;
            $this->data['view_content'] = 'code';
            $this->data['code'] = $name;
            $this->load->view('home',  $this->data);

        }
        
        function save() {
            $this->load->library('ciqrcode');
	
            $date = date('YmdHis');
            $params['data'] = 'This is a text to encode become QR Code';
            $params['level'] = 'H';
            $params['size'] = 4;
            $params['savename'] = IMGPATH.$date.'.png';
            $this->ciqrcode->generateCode($params);
            
            $this->data['username']     = $this->username;
            $this->data['name']         = $this->name;
            $this->data['view_content'] = 'code';
            $this->data['code'] = $date;
            $this->load->view('home',  $this->data);
            //echo '<img src="'.base_url().'tes.png" />';
        }
		
}
