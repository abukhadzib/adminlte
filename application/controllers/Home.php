<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'controllers/Base.php');

class Home extends Base {

	function __construct()
    {
		parent::__construct(); 
        $this->load->model('m_data_master'); 
        $this->load->model('m_pesan'); 
	}
	
	function index()
	{
		if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
        
        $data_partner    = $this->m_data_master->getPartnersCount();
        $data_partner    = json_decode($data_partner);
        
        $data_program    = $this->m_data_master->getProgramsCount();
        $data_program    = json_decode($data_program);
        
        $data_subprogram = $this->m_data_master->getSubProgramsCount();
        $data_subprogram = json_decode($data_subprogram);
        
        $data_pesan      = $this->m_pesan->getPesansCount();
        $data_pesan      = json_decode($data_pesan);
        
        $this->data['partner_count']    = $data_partner->total;
        $this->data['program_count']    = $data_program->total;
        $this->data['subprogram_count'] = $data_subprogram->total;
        $this->data['pesan_count']      = $data_pesan->total;
        $this->data['view_content']     = 'dashboard';
        
        $this->load->view('home',$this->data);
	} 
    
	function webcam()
	{
		if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
        
        
       
        $this->data['view_content']     = 'webcam';
        
        $this->load->view('home',$this->data);
	} 
	
	function email()
	{
	
		$this->load->library('apiemail');
		
		$this->apiemail->emailRedeem();
	
	   
		$this->data['view_content']     = 'dashboard';
		
		$this->load->view('home',$this->data);
	}
    
        function event()
	{
		if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
        
        $this->data['view_content']     = 'event';
        
        $this->load->view('home',$this->data);
	} 
}
