<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'controllers/Base.php');

class Chat extends Base {

	function __construct()
    {
		parent::__construct(); 
        $this->load->model('m_chat'); 
	}
	
	function index($id = '')
	{
		if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
        
        $user = $this->m_chat->getUserOnline();
        $user = json_decode($user);
        
        $data = $this->m_chat->getChat($id,$this->userid);
        $data = json_decode($data);
        
        $this->data['perpage']      = $this->limit_page;
        $this->data['username']     = $this->username;
        $this->data['name']         = $this->name;
        $this->data['other']        = $id;
        $this->data['id']           = $this->userid;   
        $this->data['content']      = $data->rows;
        $this->data['user']         = $user->rows;
        
        $this->data['view_content']     = 'chat';
        
        $this->load->view('home',$this->data);
	} 
        
     function refresh($id = '')
	{
		if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
        
        $user = $this->m_chat->getUserOnline();
        $user = json_decode($user);
        
        $data = $this->m_chat->getChat($id,$this->userid);
        $data = json_decode($data);
        
        $this->data['perpage']      = $this->limit_page;
        $this->data['username']     = $this->username;
        $this->data['name']         = $this->name;
        $this->data['other']        = $id;
        $this->data['id']           = $this->userid;   
        $this->data['content']      = $data->rows;
        $this->data['user']         = $user->rows;
        //var_dump($data);
        $this->data['view_content']     = 'chat_refresh';
        
        $this->load->view('chat_refresh',$this->data);
        
        //$this->output->set_content_type('application/json')->set_output(json_encode($data));
	} 
        
    function insert_message(){
        if ($this->input->post()){
            $data = array('user_id'=>$this->input->post('id'),
                          'comment'=>$this->input->post('message'));
            if ($this->m_chat->addComment($data)){
                echo "{success:true, Msg:'Success add comment'}";
                
            } else {
                echo "{success:false, Msg:'Failed add comment'}";
            }            
        } else {
            echo "{success:false, Msg:'Error add comment'}";
        }
    }
    
}
