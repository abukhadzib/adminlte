<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'controllers/Base.php');

class User_management extends Base 
{
    function __construct()
    {
		parent::__construct();
        $this->load->model('m_user_management'); 
        $this->load->model('m_data_master');         
        $this->load->model('m_login');  
	}
	
	function index()
	{
        if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
         
        redirect(site_url('home'));
	} 
	
	function user($page=1)
	{
        if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
        
        $page = is_numeric($page) ? $page : 1;
        $page = $page < 1 ? 1 : $page;
        $page--;
                
        $data = $this->m_user_management->getUser($this->limit_page,$page);
        $data = json_decode($data);
        
        $this->data['page']         = $page;
        $this->data['perpage']      = $this->limit_page;
        $this->data['username']     = $this->username;
        $this->data['name']         = $this->name;
        $this->data['content']      = $data->rows;
        $this->data['view_content'] = 'user_management/user';
        
        $link = site_url('user_management/user');
        $this->data['pagination'] = paging(ceil($data->total/$this->limit_page),$page+1,$link,$data->total,$this->limit_page);
        
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'user_management/user','data'=>'');
        
        if ( !$this->cekAksesAction('user_management/user','read_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';    
        }
        
        $this->addLog($data_log);
        
        $this->load->view('home',$this->data);
	} 
	
	function user_add()
	{
        if ( empty($this->username) ) {
            echo "Session anda telah berakhir, refresh browser anda.";
            exit;
	    } 
        
        if ($this->input->post())
        {
            if ( !$this->cekAksesAction('user_management/user','create_') ){
                echo "{success:false, Msg:'Anda tidak mempunyai hak menambah data User !.'}";
                exit;
            }            
            if ( $this->m_user_management->cekUser($this->input->post('username')) )
            {
                echo "{success:false, Msg:'Username telah terdaftar !.'}";
                exit;
            }
			
            $data = array( 'username'      => $this->input->post('username'),
                           'password'      => md5($this->input->post('password')),
                           'email'         => $this->input->post('email'),
                           'name'          => $this->input->post('name'),
                           'phone'         => $this->input->post('phone'),
                           'address1'      => $this->input->post('address1'),
                           'address2'      => $this->input->post('address2'),
                           'no_identitas'  => $this->input->post('no_identitas'),
                           'zip'           => $this->input->post('zipcode'),
                           'city'          => $this->input->post('city'),
                           'country'       => $this->input->post('country'),
                           'status'        => 'Active',
                           'role_id'       => $this->input->post('role'),
                           //'is_admin'      => $this->input->post('is_admin')=='on'?'Y':'N',
                           'verified'      => 'Y' //$this->input->post('verified')=='on'?'Y':'N'
                    );
                    
            $data_log = array('userid'=>$this->userid,'action'=>'Add','modul'=>'user_management/user_add','data'=>json_encode($data));
            $this->addLog($data_log);
            
            if ( $this->m_user_management->addUser($data))
            {
                echo "{success:true, Msg:'Proses tambah data User <b>Berhasil</b>'}";
                exit; 
            }    
            else
            {
                echo "{success:true, Msg:'Ada kesalahan dalam menambah data User.'}";
                exit; 
            }        
        }
        
        $this->data['content']      = '';
        $this->data['error_signup'] = $this->session->flashdata('error_signup');
        $this->data['role']         = $this->m_user_management->getRoles();
        
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'user_management/user_add','data'=>'');
        
        if ( !$this->cekAksesAction('user_management/user','create_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';    
        }
        
        $this->addLog($data_log);
                
        $this->load->view('user_management/user_add',$this->data);
	} 
	
	function user_edit($id='')
	{
        if ( empty($this->username) ) {
            echo "Session anda telah berakhir, refresh browser anda.";
            exit;
	    } 
        
        if ( empty($id) ) {
            echo "{success:false, Msg:'Parameter tidak lengkap !.'}";
            exit;
	    } 
        
        if ($this->input->post())
        {
            if ( !$this->cekAksesAction('user_management/user','update_') ){
                echo "{success:false, Msg:'Anda tidak mempunyai hak merubah data User !.'}";
                exit;
            }
            
            if ( $this->m_user_management->cekUser($this->input->post('username'),$this->input->post('user_id')) )
            {
                echo "{success:false, Msg:'Username telah terdaftar !.'}";
                exit;
            }
			
            $data = array( 'username'      => $this->input->post('username'),
                           'password'      => md5($this->input->post('password')),
                           'email'         => $this->input->post('email'),
                           'name'          => $this->input->post('name'),
                           'phone'         => $this->input->post('phone'),
                           'address1'      => $this->input->post('address1'),
                           'address2'      => $this->input->post('address2'),
                           'no_identitas'  => $this->input->post('no_identitas'),
                           'zip'           => $this->input->post('zipcode'),
                           'city'          => $this->input->post('city'),
                           'country'       => $this->input->post('country'),
                           'role_id'       => $this->input->post('role'),
                           //'is_admin'      => $this->input->post('is_admin')=='on'?'Y':'N',
                           'verified'      => 'Y' //$this->input->post('verified')=='on'?'Y':'N'
                    );
            
            $data_log = array('userid'=>$this->userid,'action'=>'Update','modul'=>'user_management/user_edit/'.$id,'data'=>json_encode($data));
            $this->addLog($data_log);
            
            if ( $this->m_user_management->updateUser($data,$this->input->post('user_id')))
            {
                echo "{success:true, Msg:'Proses rubah data User <b>Berhasil</b>'}";
                exit; 
            }    
            else
            {
                echo "{success:true, Msg:'Ada kesalahan dalam merubah data User.'}";
                exit; 
            }        
        }
        
        $this->data['content']      = '';
        $this->data['error_signup'] = $this->session->flashdata('error_signup');
        $this->data['role']         = $this->m_user_management->getRoles();
        $this->data['user']         = $this->m_user_management->getUserById($id);
        
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'user_management/user_edit/'.$id,'data'=>'');
        
        if ( !$this->cekAksesAction('user_management/user','update_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';    
        }
        
        $this->addLog($data_log);
                
        $this->load->view('user_management/user_edit',$this->data);
	}  
	
	function user_delete($id='')
	{
        if ( empty($this->username) ) {
            echo "{success:false, Msg:'Login dibutuhkan !'}";
            exit;
	    } 
        
        if ( empty($id) ) {
            echo "{success:false, Msg:'Parameter tidak lengkap !'}";
            exit;
	    } 
        
        if ($this->input->post() && $this->input->post('action')=='delete')
        {
            $data_log = array('userid'=>$this->userid,'action'=>'Delete','modul'=>'user_management/user_delete/'.$id,'data'=>'');
            
            if ( $id==$this->userid ){
                echo "{success:false, Msg:'Anda tidak berhak menghapus data User yang sedang login !'}";
                exit; 
            }
            
            if ( !$this->cekAksesAction('user_management/user','delete_') ){
                echo "{success:false, Msg:'Anda tidak berhak menghapus data User !'}";
                $data_log['data'] = "{success:false, Msg:'You have no rights to delete user !'}";
                $this->addLog($data_log);
                exit; 
            }
            
            $id = $this->input->post('id');
            if ( $this->m_user_management->deleteUser($id) ){
                echo "{success:true, Msg:'Proses hapus data User <b>Berhasil</b> !'}";
                $data_log['data'] = "{success:true, Msg:'Berhasil hapus user'}";
                $this->addLog($data_log);
            }
        } else {
            echo "{success:false, Msg:'Parameter hapus data User salah !'}";
        }
	} 
	
	function role($page=1)
	{
        if ( empty($this->username) ) {
	       redirect(site_url('login'));
	    } 
        
        $page = is_numeric($page) ? $page : 1;
        $page = $page < 1 ? 1 : $page;
        $page--;
                
        $data = $this->m_user_management->getRole($this->limit_page,$page);
        $data = json_decode($data);
        
        $this->data['page'] = $page;
        $this->data['perpage'] = $this->limit_page;
        $this->data['username']     = $this->username;
        $this->data['name']         = $this->name;
        $this->data['content']      = $data->rows;
        $this->data['view_content'] = 'user_management/role';
        
        $link = site_url('user_management/role');
        $this->data['pagination'] = paging(ceil($data->total/$this->limit_page),$page+1,$link,$data->total,$this->limit_page);
        
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'user_management/role','data'=>'');
        
        if ( !$this->cekAksesAction('user_management/role','read_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';           
        }
        
        $this->addLog($data_log);
                
        $this->load->view('home',$this->data);
	} 
	
	function role_add()
	{
        if ( empty($this->username) ) {
            echo "Session anda telah berakhir, refresh browser anda.";
            exit;
	    } 
        
        if ($this->input->post())
        {    
            if ( !$this->cekAksesAction('user_management/role','create_') ){
                echo "{success:false, Msg:'Anda tidak mempunyai hak menambah data Role !.'}";
                exit;
            }
            
            $role_name   = $this->input->post('role_name');
            $partner     = $this->input->post('partner');
            $program     = implode(',',$this->input->post('program'));
            $is_admin    = $this->input->post('is_admin');
            $role_create = $this->input->post('create');
            $role_read   = $this->input->post('read');
            $role_update = $this->input->post('update');
            $role_delete = $this->input->post('delete');
            
            $data  = array('name'=>$role_name,'is_admin'=>($is_admin=='on'?'Y':'N'),'partner_id'=>$partner,'program_id'=>$program);
            $data2 = array(); 
            $data3 = array(); 
            
            if ($role_create){
                foreach ($role_create as $key => $value) {
                    $data2[$value]['create'] = '1';
                }
            }
            if ($role_read){
                foreach ($role_read as $key => $value) {
                    $data2[$value]['read'] = '1';
                }
            }
            if ($role_update){
                foreach ($role_update as $key => $value) {
                    $data2[$value]['update'] = '1';
                }
            }
            if ($role_delete){
                foreach ($role_delete as $key => $value) {
                    $data2[$value]['delete'] = '1';
                }
            }
            
            if ( $this->m_user_management->addRole($data) )
            {
                $role_id = $this->db->insert_id();
                
                foreach ($data2 as $key => $value) 
                {                
                    $create = '0';
                    $read = '0';
                    $update = '0';
                    $delete  = '0';   
                                                 
                    foreach ($value as $key2 => $value2) 
                    {
                        $create = $key2 == 'create' ? '1' : $create;
                        $read   = $key2 == 'read'   ? '1' : $read;
                        $update = $key2 == 'update' ? '1' : $update;
                        $delete = $key2 == 'delete' ? '1' : $delete;                    
                    }
                    
                    $data3[] = array('role_id' => $role_id,
                                     'menu_id' => $key,
                                     'create_' => $create,
                                     'read_'   => $read,
                                     'update_' => $update,
                                     'delete_' => $delete);
                }
                
                $data_log = array('userid'=>$this->userid,'action'=>'Add','modul'=>'user_management/role_add','data'=>json_encode(array($data,$data3)));
                $this->addLog($data_log);
                
                if ($data3){                
                    $this->m_user_management->addRoleMenu($data3);
                } 
                
                echo "{success:true, Msg:'Proses tambah data Role <b>Berhasil</b>'}";
                exit; 
            } 
            else
            {
                echo "{success:false, Msg:'Proses tambah data Role <b>Gagal</b>'}";
                exit; 
            }            
        }
        
        $menu = $this->m_user_management->getMenu();
        $menu = json_decode($menu);
        
        $this->data['content'] = '';
        $this->data['menu']    = $menu->rows;
        $this->data['partner'] = $this->m_data_master->getPartners();         
        $this->data['program'] = '';
        
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'user_management/role_add','data'=>'');
        
        if ( !$this->cekAksesAction('user_management/role','create_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';      
        }
        
        $this->addLog($data_log);
                
        $this->load->view('user_management/role_add',$this->data);
	} 
	
	function role_edit($id='')
	{
        if ( empty($this->username) ) {
            echo "Session anda telah berakhir, refresh browser anda.";
            exit;
	    } 
        
        if ( empty($id) ) {
            echo "{success:false, Msg:'Parameter tidak lengkap !.'}";
            exit;
	    } 
        
        if ($this->input->post())
        {
            if ( !$this->cekAksesAction('user_management/role','update_') ){
                echo "{success:false, Msg:'Anda tidak mempunyai hak merubah data Role !.'}";
                exit;
            }
            
            $role_id     = $this->input->post('role_id');
            $role_name   = $this->input->post('role_name');
            $partner     = $this->input->post('partner');
            $program     = implode(',',$this->input->post('program'));
            $is_admin    = $this->input->post('is_admin');
            $role_create = $this->input->post('create');
            $role_read   = $this->input->post('read');
            $role_update = $this->input->post('update');
            $role_delete = $this->input->post('delete');
            
            $data      = array('name'=>$role_name,'is_admin'=>($is_admin=='on'?'Y':'N'),'partner_id'=>$partner,'program_id'=>$program);
            
            if ( $this->m_user_management->updateRole($data,$role_id) )
            {
                $this->m_user_management->deleteRoleMenu($role_id);
                
                $data2 = array(); 
                $data3 = array(); 
                
                if ($role_create){
                    foreach ($role_create as $key => $value) {
                        $data2[$value]['create'] = '1';
                    }
                }
                if ($role_read){
                    foreach ($role_read as $key => $value) {
                        $data2[$value]['read'] = '1';
                    }
                }
                if ($role_update){
                    foreach ($role_update as $key => $value) {
                        $data2[$value]['update'] = '1';
                    }
                }
                if ($role_delete){
                    foreach ($role_delete as $key => $value) {
                        $data2[$value]['delete'] = '1';
                    }
                }
                
                foreach ($data2 as $key => $value) 
                {                
                    $create = '0';
                    $read = '0';
                    $update = '0';
                    $delete  = '0';   
                                                 
                    foreach ($value as $key2 => $value2) 
                    {
                        $create = $key2 == 'create' ? '1' : $create;
                        $read   = $key2 == 'read'   ? '1' : $read;
                        $update = $key2 == 'update' ? '1' : $update;
                        $delete = $key2 == 'delete' ? '1' : $delete;                    
                    }
                    
                    $data3[] = array('role_id' => $role_id,
                                     'menu_id' => $key,
                                     'create_' => $create,
                                     'read_'   => $read,
                                     'update_' => $update,
                                     'delete_' => $delete);
                }
                
                $data_log = array('userid'=>$this->userid,'action'=>'Update','modul'=>'user_management/role_edit/'.$id,'data'=>json_encode(array($data,$data3)));
                $this->addLog($data_log);
                
                if ($data3){                
                    $this->m_user_management->addRoleMenu($data3);
                } 
                
                echo "{success:true, Msg:'Proses rubah data Role <b>Berhasil</b>'}";
                exit; 
            }
            else
            {
                echo "{success:false, Msg:'Proses rubah data Role <b>Gagal</b>'}";
                exit; 
            }
        }
        
        $menu = $this->m_user_management->getMenu();
        $menu = json_decode($menu);
        
        $this->data['content']   = '';
        $this->data['menu']      = $menu->rows;
        $this->data['role']      = $this->m_user_management->getRoleById($id);
        $this->data['role_menu'] = $this->m_user_management->getRoleMenuById($id);
        $this->data['partner']   = $this->m_data_master->getPartners();         
        $this->data['program']   = $this->m_data_master->getPrograms($this->data['role']->partner_id);         
        
        $data_log = array('userid'=>$this->userid,'action'=>'View','modul'=>'user_management/role_edit/'.$id,'data'=>'');
        
        if ( !$this->cekAksesAction('user_management/role','update_') ){
            $this->data['view_content'] = 'forbidden'; 
            $data_log['action'] = 'View Forbidden';    
        }
        
        $this->addLog($data_log);
                
        $this->load->view('user_management/role_edit',$this->data);
	}
	
	function role_delete($id='')
	{
        if ( empty($this->username) ) {
            echo "{success:false, Msg:'Login dibutuhkan !'}";
            exit;
	    } 
        
        if ( empty($id) ) {
            echo "{success:false, Msg:'Parameter tidak lengkap !'}";
            exit;
	    } 
        
        if ($this->input->post() && $this->input->post('action')=='delete')
        {
            $id_post = $this->input->post('id');
            
            if ( $id_post!=$id ){
                echo "{success:false, Msg:'Parameter delete salah !'}";
                exit;
            }
            
            if ( !$this->cekAksesAction('user_management/role','delete_') ){
                echo "{success:false, Msg:'Anda tidak berhak menghapus data Role !'}";
                exit; 
            }
            
            if ( $this->m_user_management->cekRoleExist($id) ){
                echo "{success:false, Msg:'Data Role masih digunakan !.'}";
                exit;
            }
            
            if ( $this->m_user_management->deleteRole($id) ){
                $this->m_user_management->deleteRoleMenu($id);
                $data_log = array('userid'=>$this->userid,'action'=>'Delete','modul'=>'user_management/role_delete/'.$id,'data'=>'');
                $this->addLog($data_log);
                echo "{success:true, Msg:'Proses hapus data role <b>Berhasil</b>'}";
            }
        } else {
            echo "{success:false, Msg:'Parameter hapus data Role salah !'}";
        }
	}  
	
	function profile()
	{
        $id = $this->userid;
        
        if ( empty($this->username) ) {
            echo "Session anda telah berakhir, refresh browser anda.";
            exit;
	    } 
        
        if ( empty($id) ) {
            echo "{success:false, Msg:'Parameter tidak lengkap !.'}";
            exit;
	    } 
        
        if ($this->input->post())
        {
            if ( $this->m_user_management->cekUser($this->input->post('username_profile'),$id) )
            {
                echo "{success:false, Msg:'Username telah terdaftar !.'}";
                exit;
            }
            
            $data = array( 'username'      => $this->input->post('username_profile'),
                           'email'         => $this->input->post('email_profile'),
                           'name'          => $this->input->post('name_profile'),
                           'phone'         => $this->input->post('phone_profile'),
                           'address1'      => $this->input->post('address1_profile'),
                           'address2'      => $this->input->post('address2_profile'),
                           'no_identitas'  => $this->input->post('no_identitas_profile'),
                           'zip'           => $this->input->post('zipcode_profile'),
                           'city'          => $this->input->post('city_profile'),
                           'country'       => $this->input->post('country_profile')
                    );
            
            $data_log = array('userid'=>$this->userid,'action'=>'Update','modul'=>'user_management/profile/','data'=>json_encode($data));
            $this->addLog($data_log);
            
            if ( $this->m_user_management->updateUser($data,$id))
            {
                echo "{success:true, Msg:'Proses rubah data Profile <b>Berhasil</b>'}";
                exit; 
            }    
            else
            {
                echo "{success:true, Msg:'Ada kesalahan dalam merubah data Profile.'}";
                exit; 
            }        
        }
        
        $this->data['content']      = '';
        $this->data['role']         = $this->m_user_management->getRoles();
        $this->data['user']         = $this->m_user_management->getUserById($id);
        
       $this->load->view('user_management/profile',$this->data);
	}  
	
	function password()
	{
        $id = $this->userid;
        
        if ( empty($this->username) ) {
            echo "Session anda telah berakhir, refresh browser anda.";
            exit;
	    } 
        
        if ( empty($id) ) {
            echo "{success:false, Msg:'Parameter tidak lengkap !.'}";
            exit;
	    } 
        
        if ($this->input->post())
        {
            $pass_old  = $this->input->post('password_old');
            $pass_new  = $this->input->post('password_new');
            $pass_konf = $this->input->post('password_konfirm');
            
            if ( $pass_new!=$pass_konf ){
                echo "{success:false, Msg:'Password Baru dan Konfirmasi Password Tidak Sama !.'}";
                exit;
            }
            
            $userpass = $this->m_login->cekUserPass($this->username,$pass_old);
            if ( !$userpass ){
                echo "{success:false, Msg:'Password Lama Salah !.'}";
                exit;
            }
            
            $data = array( 'password' => md5($pass_new) );
            
            $data_log = array('userid'=>$this->userid,'action'=>'Update','modul'=>'user_management/password/','data'=>json_encode($data));
            $this->addLog($data_log);
            
            if ( $this->m_user_management->updateUser($data,$id))
            {
                echo "{success:true, Msg:'Proses rubah data Password <b>Berhasil</b>'}";
                exit; 
            }    
            else
            {
                echo "{success:true, Msg:'Ada kesalahan dalam merubah data Password.'}";
                exit; 
            }        
        }
        
        $this->data['content']      = '';
        
       $this->load->view('user_management/password',$this->data);
	}  
    
}
