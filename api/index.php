<?php
error_reporting(0);
require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once dirname(__DIR__) . '/libs/Log/DateTimeFileWriter.php';

require_once dirname(__DIR__) . '/classes/AccountValidate.php';
require_once dirname(__DIR__) . '/classes/BalanceInfo.php';
require_once dirname(__DIR__) . '/classes/TransferInquiry.php';
require_once dirname(__DIR__) . '/classes/TransferProcess.php';
require_once dirname(__DIR__) . '/classes/Reversal.php';

$app = new \Slim\Slim(array(
    'log.enabled' => true,
    'log.level' => \Slim\Log::INFO,
    'log.writer' => new \Slim\Extras\Log\DateTimeFileWriter(array(
        'path' => dirname(__DIR__) . '/logs',
        'name_prefix' => 'log_',
        'name_format' => 'Y-m-d',
        'message_format' => '%label% [%date%] %message%'
            ))
        ));

$app->post('/validate_account', function () use ($app) {
    $app->getLog()->info('RECV from TAPS: ' . json_encode($_POST));
    $obj = new AccountValidate();
    $obj->execute($app->request->headers->all(), $app->request->params());
});

$app->post('/getbalance', function () use ($app) {
    $app->getLog()->info('RECV from TAPS: ' . json_encode($_POST));
    $obj = new BalanceInfo();
    $obj->execute($app->request->headers->all(), $app->request->params());
});

$app->post('/deductinquiry', function () use ($app) {
    $obj = new TransferInquiry();
    $obj->execute($app->request->headers->all(), $app->request->params());
});

$app->post('/deduct', function () use ($app) {
    $obj = new TransferProcess();
    $obj->execute($app->request->headers->all(), $app->request->params());
});

$app->post('/reversal', function () use ($app) {
    $obj = new Reversal();
    $obj->execute($app->request->headers->all(), $app->request->params());
});

$app->run();
